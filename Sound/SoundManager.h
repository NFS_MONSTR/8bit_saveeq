//
// Created by nfs_monstr on 14.08.18.
//

#ifndef INC_8BIT_SAVEEQ_SOUNDMANAGER_H
#define INC_8BIT_SAVEEQ_SOUNDMANAGER_H


#include <SDL_mixer.h>
#include <string>
#include <map>

class SoundManager {
public:
    SoundManager() = default;
    SoundManager(std::string musicConfig, std::string soundConfig);
    void loadMusic(std::string musicConfig);
    void loadSounds(std::string soundConfig);
    ~SoundManager();

    /* Music */
    void playMusic(std::string name, int times = -1);
    void resumeMusic();
    void pauseMusic();
    void stopMusic();
    void setMusicVolume(int volume);
    int getMusicVolume();
    bool isMusicPlaying();
    bool isMusicPaused();

    /* Sfx */
    void playSound(std::string name, int channel = -1);
    void resumeSound(int channel = -1);
    void pauseSound(int channel = -1);
    void stopSound(int channel = -1);
    void setSoundVolume(int volume);
    int getSoundVolume();
    int isSoundPlaying(int channel = -1);
    int isSoundPaused(int channel = -1);



private:
    std::map<std::string, Mix_Music*> music;
    std::map<std::string, Mix_Chunk*> sounds;
};


#endif //INC_8BIT_SAVEEQ_SOUNDMANAGER_H
