//
// Created by nfs_monstr on 14.08.18.
//

#include <rapidjson/istreamwrapper.h>
#include <fstream>
#include <rapidjson/document.h>
#include "SoundManager.h"

SoundManager::SoundManager(std::string musicConfig, std::string soundConfig) {
    if (!musicConfig.empty())
        loadMusic(std::move(musicConfig));
    if (!soundConfig.empty())
        loadSounds(std::move(soundConfig));
}

SoundManager::~SoundManager() {
    for (const auto& sound : sounds)
        Mix_FreeChunk(sound.second);
    for (const auto& mus : music)
        Mix_FreeMusic(mus.second);
}

void SoundManager::loadMusic(std::string musicConfig) {
    std::ifstream ifstream(musicConfig);
    rapidjson::IStreamWrapper iStreamWrapper(ifstream);
    rapidjson::Document musicConfiguration;
    musicConfiguration.ParseStream(iStreamWrapper);
    for (const auto &music : musicConfiguration["music"].GetArray()) {
        auto musicPtr = Mix_LoadMUS(music["path"].GetString());
        this->music.insert(std::pair<std::string, Mix_Music *>(music["name"].GetString(), musicPtr));
    }

}

void SoundManager::loadSounds(std::string soundConfig) {
    std::ifstream ifstream(soundConfig);
    rapidjson::IStreamWrapper iStreamWrapper(ifstream);
    rapidjson::Document soundConfiguration;
    soundConfiguration.ParseStream(iStreamWrapper);
    for (const auto &sound : soundConfiguration["sounds"].GetArray()) {
        auto soundPtr = Mix_LoadWAV(sound["path"].GetString());
        this->sounds.insert(std::pair<std::string, Mix_Chunk *>(sound["name"].GetString(), soundPtr));
    }
}

void SoundManager::playMusic(std::string name, int times) {
    Mix_PlayMusic(music[name], times);
}

void SoundManager::resumeMusic() {
    Mix_ResumeMusic();
}

void SoundManager::pauseMusic() {
    Mix_PauseMusic();
}

void SoundManager::stopMusic() {
    Mix_HaltMusic();
}

void SoundManager::setMusicVolume(int volume) {
    Mix_VolumeMusic(volume);
}

int SoundManager::getMusicVolume() {
    return Mix_VolumeMusic(-1);
}

bool SoundManager::isMusicPlaying() {
    return (bool)Mix_PlayingMusic();
}

bool SoundManager::isMusicPaused() {
    return (bool)Mix_PausedMusic();
}

void SoundManager::playSound(std::string name, int channel) {
    Mix_PlayChannel(channel, sounds[name], 0);
}

void SoundManager::resumeSound(int channel) {
    Mix_Resume(channel);
}

void SoundManager::pauseSound(int channel) {
    Mix_Pause(channel);
}

void SoundManager::stopSound(int channel) {
    Mix_HaltChannel(channel);
}

void SoundManager::setSoundVolume(int volume) {
    Mix_Volume(-1, volume);
}

int SoundManager::getSoundVolume() {
    return Mix_Volume(-1 ,-1);
}

int SoundManager::isSoundPlaying(int channel) {
    return Mix_Playing(channel);
}

int SoundManager::isSoundPaused(int channel) {
    return Mix_Paused(channel);
}