#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include "Lib/SDL_anigif.h"
#include "Textures/Texture.h"
#include <iostream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <fstream>

#include "Textures/Animations.h"
#include "Objects/Level.h"
#include "Objects/Entity.h"
#include "Config/Config.h"
#include "Textures/Sprite.h"
#include "Objects/Player.h"
#include "build_defs.h"
#include "Menus/StartScreen.h"
#include "Menus/LevelIntro.h"
#include "Menus/GameGifIntro.h"
#include "Menus/SelectPlayer.h"
#include "Textures/ImageManager.h"
#include "Text/FontManager.h"
#include "Text/TextManager.h"
#include "Sound/SoundManager.h"

SDL_Window *window = nullptr;
SDL_Renderer *renderer = nullptr;

int window_width = 764;
int window_height = 720;
const int logical_height = 720;
const int original_width = 764;
int logical_width = 764;

Config gameConfig("Resources/game.cfg");

Config imagesCfg("Resources/Images/images.cfg");
ImageManager *imgMgr = nullptr;
FontManager *fontManager = nullptr;
TextManager *textManager = nullptr;
SoundManager *soundManager = nullptr;

enum players {
    RR = 0,
    FS,
    AJ,
    PP,
    RD,
    TS
};

bool init_window() {

    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG);
    if (TTF_Init()==-1) {
        std::cout << "Cant init SDL_ttf: " << TTF_GetError() << std::endl;
        return false;
    }

    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048)==-1) {
        std::cout << "Cant init SDL_mixer: " << Mix_GetError() << std::endl;
    }
    int mix_flags = MIX_INIT_OGG;
    if (Mix_Init(mix_flags)!=mix_flags) {
        std::cout << "Cant init SDL_mixer ogg library: " << Mix_GetError() << std::endl;
        return false;
    }

    std::string build_month = std::to_string(BUILD_MONTH).length()<2 ? "0"+std::to_string(BUILD_MONTH) : std::to_string(BUILD_MONTH);
    std::string build_day = std::to_string(BUILD_DAY).length()<2 ? "0"+std::to_string(BUILD_DAY) : std::to_string(BUILD_DAY);

    if (gameConfig.isInt("window_width") && gameConfig.isInt("window_height")) {
        window_width = gameConfig.getInt("window_width");
        window_height = gameConfig.getInt("window_height");
        logical_width = int(round(float(window_width)/window_height*logical_height));
    }

    window = SDL_CreateWindow((std::string("8bit_SaveEq [build ")+std::to_string(BUILD_YEAR)+build_month+build_day+std::string("]")).c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        std::cout << "[Error]Can't create window: " << SDL_GetError() << std::endl;
        return false;
    }

    Uint32 rendererFlags = 0;

    if (gameConfig.isInt("useSoftware"))
        rendererFlags |= gameConfig.getInt("useSoftware")!=0 ? SDL_RENDERER_SOFTWARE : SDL_RENDERER_ACCELERATED;


    renderer = SDL_CreateRenderer(window, -1, rendererFlags);
    if (renderer == nullptr) {
        std::cout << "[Error]Can't create renderer: " << SDL_GetError() << std::endl;
        return false;
    }

    SDL_RenderSetLogicalSize(renderer, logical_width, logical_height);

    return true;
}

void done_window() {
    if (renderer != nullptr)
        SDL_DestroyRenderer(renderer);
    if (window != nullptr)
        SDL_DestroyWindow(window);
    Mix_CloseAudio();
    Mix_Quit();
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

Animations *player_anim[6];

Level *level = nullptr;

void loadImage(const std::string name) {
    imgMgr->addImage(imagesCfg.getString(name),name);
}

void loadAnimation(std::string name, const std::string prefix, int start, int count, int delay, AnimationType animationType = AnimationType::Standing) {
    auto animation = new Animations();
    for (int i = start; i < start+count; ++i)
        animation->addAnimation(animationType, renderer, imagesCfg.getString(prefix+std::to_string(i)), "");
    animation->setDelay(animationType, delay);
    imgMgr->addAnimation(animation,std::move(name));
}


void load_images() {//todo rewrite image loading to json file

    imgMgr = new ImageManager(renderer);

    loadImage("bg_level_1-1_0");
    loadAnimation("start_screen","start_screen_",0,2,500,AnimationType::Standing);

    auto selectPlayerImgs = new Animations();
    selectPlayerImgs->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("player_selector_bg"));
    for (int i = 0; i<6; i++)
        selectPlayerImgs->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("selected_player_"+std::to_string(i)));
    imgMgr->addAnimation(selectPlayerImgs, "player_selector");

    loadAnimation("level_intro","level_intro_",0,3,500,AnimationType::Standing);


    loadAnimation("coin", "coin_", 0, 6, 100);
    loadAnimation("chest", "chest_", 0, 2, 0);
    loadAnimation("question", "question_", 0, 2, 0);

    auto rd_bonus = new Animations();
    rd_bonus->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("rd_bonus"), "");
    rd_bonus->setDelay(AnimationType::Standing, 1000);
    imgMgr->addAnimation(rd_bonus,"rd_bonus");

    //load spikes sprites
    auto spikes = new Animations();
    for (int i = 0; i < 4; ++i)
        spikes->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("spikes_"+std::to_string(i)), "");
    spikes->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("spikes_2"), "");//to make smooth loop
    spikes->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("spikes_1"), "");
    spikes->setDelay(AnimationType::Standing, 200);
    imgMgr->addAnimation(spikes, "spikes");

    auto dragon = new Animations();
    for (int i = 0; i < 3; ++i)
        dragon->addAnimation(AnimationType::Flying, renderer, imagesCfg.getString("dragon_flying_"+std::to_string(i)),"");
    dragon->addAnimation(AnimationType::Jumping, renderer, imagesCfg.getString("dragon_falling_0"), "");
    dragon->setDelay(AnimationType::Flying, 180);
    imgMgr->addAnimation(dragon, "dragon");


    auto tirek = new Animations();
    for (int i = 0; i < 3; ++i)
        tirek->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("tirek_standing_"+std::to_string(i)),"");
    tirek->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("tirek_standing_1"),"");
    tirek->setDelay(AnimationType::Standing, 400);
    for (int i = 0; i < 4; ++i)
        tirek->addAnimation(AnimationType::Walking, renderer, imagesCfg.getString("tirek_walking_"+std::to_string(i)),"");
    tirek->setDelay(AnimationType::Walking, 300);
    imgMgr->addAnimation(tirek, "tirek");

    //load player
    player_anim[RD] = new Animations();//todo put this into json
    for (char i = 0; i < 3; i++)
        player_anim[RD]->addAnimation(AnimationType::Standing, renderer, imagesCfg.getString("rd_standing_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::Standing, 400);

    for (char i = 0; i < 5; i++)
        player_anim[RD]->addAnimation(AnimationType::Walking, renderer, imagesCfg.getString("rd_walking_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::Walking, 100);

    for (char i = 0; i < 3; i++)
        player_anim[RD]->addAnimation(AnimationType::Jumping, renderer, imagesCfg.getString("rd_jumping_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::Jumping, 0);

    for (int i = 0; i < 4; i++)
        player_anim[RD]->addAnimation(AnimationType::Flying, renderer, imagesCfg.getString("rd_flying_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::Flying, 150);

    for (int i = 0; i < 5; i++)
        player_anim[RD]->addAnimation(AnimationType::Prepare_PowerUp, renderer, imagesCfg.getString("rd_prepare_powerup_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::Prepare_PowerUp, 200);

    for (int i = 0; i < 12; i++)
        player_anim[RD]->addAnimation(AnimationType::PowerUp, renderer, imagesCfg.getString("rd_powerup_"+std::to_string(i)), "");
    player_anim[RD]->setDelay(AnimationType::PowerUp, 100);

    for (int i = 0; i < 1; i++)
        player_anim[RD]->addAnimation(AnimationType::Attacking, renderer, imagesCfg.getString("rd_attack_front_"+std::to_string(i)), "");


    imgMgr->addAnimation(player_anim[RD], "player_4");

    Texture *zero_block = new Texture(renderer);
    zero_block->setSolid(false);
    level->addBlock(zero_block);
    for (int i = 1; i < 21; ++i)
        level->addBlock(new Sprite(renderer, imagesCfg.getString("block_" + std::to_string(i)), ""));
}

void load_fonts() {
    fontManager = new FontManager(renderer);
    std::ifstream ifstream("Resources/Fonts/fonts.json");
    rapidjson::IStreamWrapper iStreamWrapper(ifstream);
    rapidjson::Document fontConfiguration;
    fontConfiguration.ParseStream(iStreamWrapper);
    for(const auto& font : fontConfiguration["fonts"].GetArray())
        fontManager->loadFont(font["path"].GetString(), font["name"].GetString(), font["size"].GetInt());
}

void load_texts() {
    textManager = new TextManager(fontManager, "Resources/texts.json");
}

void load_sound() {
    soundManager = new SoundManager("Resources/Music/music.json", "Resources/Sfx/sfx.json");
    if (gameConfig.isInt("music_volume"))
        soundManager->setMusicVolume(gameConfig.getInt("music_volume"));
    if (gameConfig.isInt("sfx_volume"))
        soundManager->setSoundVolume(gameConfig.getInt("sfx_volume"));
}

void load_resources() {
    load_images();
    load_fonts();
    load_texts();
    load_sound();
}

void done_resources() {
    delete soundManager;
    delete textManager;
    delete fontManager;
    delete imgMgr;
}

int player_type;

void level_loop() {

    level->setBackground(imgMgr->getImage("bg_level_1-1_0"));
    level->setRenderer(renderer);
    level->setImageManager(imgMgr);
    level->setTextManager(textManager);
    level->setSoundManager(soundManager);

    level->loadMap("Resources/map.bin");

    level->run();

}

void quit_game() {
    done_resources();
    done_window();
}

#ifdef _WIN32//Костыль, чтобы на винде не создавалось окно с консолью
#include "windows.h"
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#else
int main() {
#endif
    if (init_window()) {
        level = new Level(logical_width);
        load_resources();
        GameGifIntro gifIntro(renderer, imagesCfg.getString("intro"));
        gifIntro.setSoundManager(soundManager);
        StartScreen startScreen(renderer, imgMgr->getAnimation("start_screen"));
        startScreen.setSoundManager(soundManager);
        SelectPlayer selectPlayer(renderer, imgMgr->getAnimation("player_selector"));
        selectPlayer.setSoundManager(soundManager);
        LevelIntro levelIntro(renderer, imgMgr->getAnimation("level_intro"));
        levelIntro.setTextManager(textManager);
        levelIntro.setLevel(1,1);

        if (gifIntro.run() == Menu_Ok && startScreen.run() == Menu_Ok && selectPlayer.run() == Menu_Ok) {
            imgMgr->destroyAnimation("start_screen"); imgMgr->destroyAnimation("player_selector");

            player_type = selectPlayer.getPlayer();
            if (levelIntro.run() == Menu_Ok) {
                level_loop();
            }
        }
        delete level;
        quit_game();
    }
    return 0;
}
