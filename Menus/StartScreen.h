//
// Created by nfs_monstr on 01.08.18.
//

#ifndef INC_8BIT_SAVEEQ_STARTSCREEN_H
#define INC_8BIT_SAVEEQ_STARTSCREEN_H


#include "AnimationMenu.h"

class StartScreen : public AnimationMenu {
public:
    using AnimationMenu::AnimationMenu;
    MenuExitStatus run() override;
    void handleEvent(SDL_Event &e) override;
private:
    void renderStopAnim();
};


#endif //INC_8BIT_SAVEEQ_STARTSCREEN_H
