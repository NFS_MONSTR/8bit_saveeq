//
// Created by nfs_monstr on 01.08.18.
//

#include <iostream>
#include "GameGifIntro.h"
#include "../Lib/SDL_anigif.h"

GameGifIntro::GameGifIntro(SDL_Renderer *renderer, std::string path) : Menu(renderer) {
    setGif(std::move(path));
}

void GameGifIntro::setGif(std::string path) {
    framesCount = AG_LoadGIF(path.c_str(), nullptr, 0);
    if (framesCount>0) {
        frames = new AG_Frame[framesCount];
        AG_LoadGIF(path.c_str(), frames, framesCount);
        AG_NormalizeSurfacesToDisplayFormat(frames, framesCount);
        int x = centerX - frames[0].surface->w/2;
        int y = centerY - frames[0].surface->h/2;
        renderQuad = {x, y, frames[0].surface->w, frames[0].surface->h};
        texture = SDL_CreateTextureFromSurface(renderer, frames[animationOffset].surface);
    } else {
        std::cout << "[Error] gif " << path << " doesn't exist" << std::endl;
        stopMenu(Menu_Ok);
    }
}

void GameGifIntro::render() {
    SDL_RenderCopy(renderer,texture, nullptr,&renderQuad);
    nextFrame();
}

MenuExitStatus GameGifIntro::run() {
    soundManager->playMusic("intro", 1);
    MenuExitStatus retval = Menu::run();
    if (soundManager->isMusicPlaying())
        soundManager->stopMusic();
    AG_FreeSurfaces(frames, framesCount);
    delete [] frames;
    return retval;
}

void GameGifIntro::handleEvent(SDL_Event &e) {
    Menu::handleEvent(e);
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
                case SDLK_RETURN:
                    stopMenu(Menu_Ok);
                    break;
            }
            break;
    }
}

void GameGifIntro::nextFrame() {
    if (SDL_GetTicks()-lastFrameUpdate>=frames[animationOffset].delay) {
        SDL_DestroyTexture(texture);
        animationOffset = animationOffset + 1;
        lastFrameUpdate = SDL_GetTicks();
        if (animationOffset >= framesCount) {
            stopMenu(Menu_Ok);
            animationOffset = 0;
        } else {
            texture = SDL_CreateTextureFromSurface(renderer, frames[animationOffset].surface);
        }
    }
}