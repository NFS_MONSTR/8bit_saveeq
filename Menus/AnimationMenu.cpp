//
// Created by nfs_monstr on 01.08.18.
//

#include "AnimationMenu.h"

AnimationMenu::AnimationMenu(Animations *a) {
    animation = a;
}

AnimationMenu::AnimationMenu(SDL_Renderer *renderer, Animations *a) : Menu(renderer) {
    animation = a;
}

MenuExitStatus AnimationMenu::run() {
    lastAnimChange = SDL_GetTicks();
    return Menu::run();
}

void AnimationMenu::render() {
    Texture *anim = animation->getAnimation(AnimationType::Standing, animationOffset);
    int x = centerX-anim->getWidth()/2, y = centerY-anim->getHeight()/2;
    anim->render(x,y);
    nextFrame();
}

void AnimationMenu::setAnimation(Animations *a) {
    animation = a;
}

void AnimationMenu::nextFrame() {
    if (SDL_GetTicks()-lastAnimChange>=animation->getDelay()) {
        animationOffset = (animationOffset + 1) % animation->getAnimaionCount();
        lastAnimChange = SDL_GetTicks();
    }
}