//
// Created by nfs_monstr on 01.08.18.
//

#ifndef INC_8BIT_SAVEEQ_LEVELINTRO_H
#define INC_8BIT_SAVEEQ_LEVELINTRO_H


#include "AnimationMenu.h"

class LevelIntro : public AnimationMenu {
public:
    using AnimationMenu::AnimationMenu;
    void handleEvent(SDL_Event &e) override;
    void nextFrame() override;
    void render() override;
    void setLevel(int chapter, int level);
    void setLevelName(std::string levelName);

protected:
    std::string levelString = "1-1";
    std::string levelName = "PONYVILLE";
};


#endif //INC_8BIT_SAVEEQ_LEVELINTRO_H
