//
// Created by nfs_monstr on 01.08.18.
//

#ifndef INC_8BIT_SAVEEQ_GAMEGIFINTRO_H
#define INC_8BIT_SAVEEQ_GAMEGIFINTRO_H


#include "Menu.h"
#include "../Lib/SDL_anigif.h"
#include <string>

class GameGifIntro : public Menu {
public:
    GameGifIntro() = default;
    explicit GameGifIntro(SDL_Renderer *renderer, std::string path);
    void setGif(std::string path);

    void render() override;
    MenuExitStatus run() override;
    void handleEvent(SDL_Event &e) override;
    void nextFrame();

protected:
    AG_Frame* frames = nullptr;
    int framesCount = 0;
    int lastFrameUpdate = 0;
    int animationOffset = 0;
    SDL_Rect renderQuad = {0, 0, 0, 0};
    SDL_Texture *texture = nullptr;
};


#endif //INC_8BIT_SAVEEQ_GAMEGIFINTRO_H
