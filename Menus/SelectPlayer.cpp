//
// Created by nfs_monstr on 02.08.18.
//

#include "SelectPlayer.h"

SelectPlayer::SelectPlayer(SDL_Renderer *renderer, Animations *images) : Menu(renderer) {
    this->images = images;
}

void SelectPlayer::setImages(Animations *a) {
    images = a;
}

void SelectPlayer::render() {
    Texture *bg = images->getAnimation(AnimationType::Standing, 0);
    bg->render(centerX-bg->getWidth()/2,centerY-bg->getHeight()/2);
    Texture *player = images->getAnimation(AnimationType::Standing, players[currenti][currentj]+1);
    player->render(centerX-player->getWidth()/2,centerY-player->getHeight()/2);
}

void SelectPlayer::handleEvent(SDL_Event &e) {
    Menu::handleEvent(e);
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
                case SDLK_RETURN:
                    if (players[currenti][currentj] == 4) {
                        selected_player = players[currenti][currentj];
                        stopMenu(Menu_Ok);
                        soundManager->playSound("menu_select");
                    }
                    break;
                case SDLK_LEFT:
                    currentj--;
                    if (currentj < 0) currentj = 3+currentj;
                    soundManager->playSound("menu_select");
                    break;
                case SDLK_RIGHT:
                    currentj = (currentj + 1) % 3;
                    soundManager->playSound("menu_select");
                    break;
                case SDLK_UP:
                    currenti--;
                    if (currenti < 0) currenti = 2+currenti;
                    soundManager->playSound("menu_select");
                    break;
                case SDLK_DOWN:
                    currenti = (currenti + 1) % 2;
                    soundManager->playSound("menu_select");
                    break;
            }
            break;
    }
}

int SelectPlayer::getPlayer() {
    return selected_player;
}
