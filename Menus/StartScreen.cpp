//
// Created by nfs_monstr on 01.08.18.
//

#include "StartScreen.h"

void StartScreen::handleEvent(SDL_Event &e) {
    AnimationMenu::handleEvent(e);
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
                case SDLK_RETURN:
                    soundManager->playSound("menu_start");
                    renderStopAnim();
                    stopMenu(Menu_Ok);
                    break;
            }
            break;
    }
}

MenuExitStatus StartScreen::run() {
    soundManager->playSound("menu_start");
    return AnimationMenu::run();
}

void StartScreen::renderStopAnim() {
    int off = animationOffset;
    for (int i = 0; i < 3; i++) {
        off = 1-off;
        animationOffset = off;
        render();
        SDL_RenderPresent(renderer);
        SDL_Delay(200);
    }
}