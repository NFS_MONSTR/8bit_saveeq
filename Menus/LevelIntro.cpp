//
// Created by nfs_monstr on 01.08.18.
//

#include "LevelIntro.h"

void LevelIntro::handleEvent(SDL_Event &e) {
    AnimationMenu::handleEvent(e);
    switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
                case SDLK_RETURN:
                    stopMenu(Menu_Ok);
                    e.type = SDL_FIRSTEVENT;
                    break;
            }
            break;
    }
}

void LevelIntro::nextFrame() {
    int prev = animationOffset;
    AnimationMenu::nextFrame();
    if (prev!=animationOffset && animationOffset == 0) {//Чтобы интро не зацикливалось
        stop = true;
        exitStatus = Menu_Ok;
    }
}

void LevelIntro::render() {
    int cur_frame = animationOffset;
    AnimationMenu::render();
    if (cur_frame==2) {
        SDL_Color level_color = {212, 78, 15, 255};

        std::string level = "LEVEL "+levelString;

        textManager->renderTextDirect(centerX-181, 299, "level_1", level, level_color, true, true);

        textManager->renderTextDirect(centerX-181, 358, "level_1", "PONYVILLE", level_color, true, true);

        textManager->renderTextDirect(centerX+184, 66, "level_2", levelString, level_color, true, true);

    }
}

void LevelIntro::setLevel(int chapter, int level) {
    levelString = std::to_string(chapter)+"-"+std::to_string(level);
}

void LevelIntro::setLevelName(std::string levelName) {
    this->levelName = levelName;
}