//
// Created by nfs_monstr on 02.08.18.
//

#ifndef INC_8BIT_SAVEEQ_SELECTPLAYER_H
#define INC_8BIT_SAVEEQ_SELECTPLAYER_H


#include "Menu.h"
#include "../Textures/Animations.h"

class SelectPlayer : public Menu{
public:
    SelectPlayer() = default;
    SelectPlayer(SDL_Renderer *renderer, Animations *images);

    void setImages(Animations *a);

    void render() override;
    void handleEvent(SDL_Event &e) override;
    int getPlayer();

protected:
    Animations *images = nullptr;
    int selected_player = 0;
    int currenti = 0, currentj = 0;
    int players[2][3] = {{0,1,2},{3,4,5}};;


};


#endif //INC_8BIT_SAVEEQ_SELECTPLAYER_H
