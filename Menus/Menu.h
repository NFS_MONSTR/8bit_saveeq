//
// Created by nfs_monstr on 01.08.18.
//

#ifndef INC_8BIT_SAVEEQ_MENU_H
#define INC_8BIT_SAVEEQ_MENU_H


#include <SDL2/SDL.h>
#include "../Text/TextManager.h"
#include "../Sound/SoundManager.h"

enum MenuExitStatus {
    Menu_Ok = 0,
    Menu_Quit
};

class Menu {
public:
    Menu() = default;
    explicit Menu(SDL_Renderer *renderer);
    void setRenderer(SDL_Renderer *renderer);
    void setTextManager(TextManager *textManager);
    void setSoundManager(SoundManager *soundManager);
    virtual void render();
    virtual MenuExitStatus run();
    virtual void handleEvent(SDL_Event &e);
    void stopMenu(MenuExitStatus status);

protected:
    bool stop = false;
    int centerX = 0,centerY = 0;
    MenuExitStatus exitStatus = Menu_Ok;
    SDL_Renderer *renderer = nullptr;
    TextManager *textManager = nullptr;
    SoundManager *soundManager = nullptr;


};


#endif //INC_8BIT_SAVEEQ_MENU_H
