//
// Created by nfs_monstr on 01.08.18.
//

#ifndef INC_8BIT_SAVEEQ_ANIMATIONMENU_H
#define INC_8BIT_SAVEEQ_ANIMATIONMENU_H


#include "Menu.h"
#include "../Textures/Animations.h"

class AnimationMenu : public Menu {
public:
    AnimationMenu() = default;
    explicit AnimationMenu(Animations *a);
    explicit AnimationMenu(SDL_Renderer *renderer, Animations *a = nullptr);
    MenuExitStatus run() override;
    void render() override;
    void setAnimation(Animations *a);
    virtual void nextFrame();
protected:
    Animations *animation = nullptr;
    int animationOffset = 0;
    int lastAnimChange = 0;

};


#endif //INC_8BIT_SAVEEQ_ANIMATIONMENU_H
