//
// Created by nfs_monstr on 01.08.18.
//

#include <SDL2/SDL.h>
#include "Menu.h"

Menu::Menu(SDL_Renderer *renderer) {
    setRenderer(renderer);
}

void Menu::setRenderer(SDL_Renderer *renderer) {
    this->renderer = renderer;
    SDL_RenderGetLogicalSize(renderer, &centerX, &centerY);
    centerX/=2;
    centerY/=2;
}

void Menu::setTextManager(TextManager *textManager) {
    this->textManager = textManager;
}

void Menu::setSoundManager(SoundManager *soundManager) {
    this->soundManager = soundManager;
}

void Menu::render() {}

MenuExitStatus Menu::run() {
    SDL_Event e;
    while (!stop) {
        render();
        while (SDL_PollEvent(&e) && !stop)
            handleEvent(e);
        if (renderer!= nullptr)
            SDL_RenderPresent(renderer);
        SDL_Delay(20);
    }
    return exitStatus;
}

void Menu::handleEvent(SDL_Event &e) {
    switch (e.type) {
        case SDL_QUIT:
            stopMenu(Menu_Quit);
            e.type = SDL_FIRSTEVENT;//clear event
            break;
    }
}

void Menu::stopMenu(MenuExitStatus status) {
    stop = true;
    exitStatus = status;
}