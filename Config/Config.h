//
// Created by nfs_monstr on 27.07.18.
//

#ifndef INC_8BIT_SAVEEQ_CONFIG_H
#define INC_8BIT_SAVEEQ_CONFIG_H


#include <string>
#include <map>

class Config {
public:
    Config() = default;
    Config(std::string name);
    void load(std::string path);
    bool isExist(std::string key);
    bool isInt(std::string key);
    int getInt(std::string key);
    bool setIntIfExists(std::string key, int &result);
    bool setBoolIntIfExists(std::string key, bool &result);
    std::string getString(std::string key);
    std::map<std::string,std::string> &getAll();


private:
    std::map<std::string,std::string> values;

};


#endif //INC_8BIT_SAVEEQ_CONFIG_H
