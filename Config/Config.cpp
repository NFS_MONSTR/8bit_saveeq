//
// Created by nfs_monstr on 27.07.18.
//

#include <fstream>
#include <iostream>
#include "Config.h"

Config::Config(std::string name) {
    load(std::move(name));
}

void Config::load(std::string path) {
    std::ifstream f(path.c_str());
    if ((f.good())) {
        std::string line;
        for (f >> line; !f.eof(); f >> line) {
            if (line.length()>=3 && line[0]!=';') {
                auto p = line.find('=');
                values.insert(
                        std::pair<std::string, std::string>(line.substr(0, p), line.substr(p + 1, line.length() - p)));
            }
        }
    } else {
        std::cout << "[Warning] Config file " << path << " doesn't exists" << std::endl;
    }

}

bool Config::isExist(std::string key) {
    return values.find(key) != values.end();
}

bool Config::isInt(std::string key) {
    try {
        auto val = values.find(key);
        if (val != values.end())
            std::stoi(values.find(key)->second);
        else
            return false;
    } catch (const std::invalid_argument& e) {
        return false;
    }
    return true;
}

bool Config::setIntIfExists(std::string key, int& result) {
    if (isExist(key) && isInt(key)) {
        result = getInt(key);
        return true;
    }
    return false;
}

bool Config::setBoolIntIfExists(std::string key, bool &result) {
    if (isExist(key) && isInt(key)) {
        result = getInt(key)!=0;
        return true;
    }
    return false;
}

int Config::getInt(std::string key) {
    return std::stoi(values.find(key)->second);
}

std::string Config::getString(std::string key) {
    return values.find(key)->second;
}

std::map<std::string,std::string> &Config::getAll() {
    return values;
}