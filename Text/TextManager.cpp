//
// Created by nfs_monstr on 10.08.18.
//

#include <fstream>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/document.h>
#include "TextManager.h"
#include "Font.h"
#include "../Objects/Entity.h"

TextManager::TextManager(FontManager *fontManager, Config *texts) {
    this->fontManager = fontManager;
    prepareTexts(texts);
}

TextManager::TextManager(FontManager *fontManager, std::string textsPath) {
    this->fontManager = fontManager;
    std::ifstream ifstream(textsPath);
    rapidjson::IStreamWrapper iStreamWrapper(ifstream);
    rapidjson::Document textConfiguration;
    textConfiguration.ParseStream(iStreamWrapper);
    for (const auto &text : textConfiguration["texts"].GetArray()) {
        auto texture = (*fontManager)[text["font"].GetString()]->prepareText(text["text"].GetString(),
                                                                             text["color_r"].GetInt(),
                                                                             text["color_g"].GetInt(),
                                                                             text["color_b"].GetInt(),
                                                                             text["color_a"].GetInt(),
                                                                             text["blended"].GetBool());
        preparedTexts.insert(std::pair<std::string, Texture *>(text["name"].GetString(), texture));
    }

}

TextManager::~TextManager() {
    for (const auto& item : preparedTexts) {
        delete item.second;
    }
}

Texture* TextManager::getPreparedText(std::string name) {
    return preparedTexts[name];
}

Texture* TextManager::operator[](std::string name) {
    return preparedTexts[name];
}

void TextManager::renderTextDirect(int x, int y, std::string font, std::string text, bool centeredX, bool centeredY) {
    renderTextDirect(x, y, std::move(font), std::move(text), {0,0,0,0}, centeredX, centeredY);
}

void TextManager::renderTextDirect(int x, int y, std::string font, std::string text, SDL_Color color, bool centeredX,
                                   bool centeredY) {
    auto texture = (*fontManager)[font]->prepareText(std::move(text), color, false);
    if (!centeredX && !centeredY)
        texture->render(x, y);
    else
    if (centeredX && centeredY)
        texture->renderCentered(x,y);
    else
    if (centeredX)
        texture->renderCentered(x,y+texture->getCenterY(Direction::Right));
    else
        texture->renderCentered(x+texture->getCenterX(Direction::Right),y);

    delete texture;
}

int TextManager::textWidth(std::string font, std::string text) {
    return (*fontManager)[font]->textWidth(std::move(text));
}

void TextManager::prepareTexts(Config *texts) {
    auto text = texts->getAll();
    for (const auto& item : text) {
        auto texture = (*fontManager)["gui_1"]->prepareText(item.second, {0, 0, 0, 255}, false);
        preparedTexts.insert(std::pair<std::string,Texture*>(item.first, texture));
    }
}