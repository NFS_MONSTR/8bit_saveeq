//
// Created by nfs_monstr on 09.08.18.
//

#include "FontManager.h"

FontManager::FontManager(SDL_Renderer *renderer) {
    this->renderer = renderer;
}

FontManager::~FontManager() {
    for (auto item : fonts)
        delete item.second;
}

void FontManager::loadFont(std::string path, std::string name, int size) {
    auto font = new Font(renderer, std::move(path), size);
    fonts.insert(std::pair<std::string,Font*>(name, font));
}

void FontManager::doneFont(std::string name) {
    delete fonts[name];
}