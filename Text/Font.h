//
// Created by nfs_monstr on 09.08.18.
//

#ifndef INC_8BIT_SAVEEQ_FONT_H
#define INC_8BIT_SAVEEQ_FONT_H

#include <string>
#include <SDL_ttf.h>
#include "../Textures/Texture.h"

class Font {
public:
    explicit Font(SDL_Renderer *renderer) ;
    ~Font();
    Font(SDL_Renderer *renderer, TTF_Font *font);
    Font(SDL_Renderer *renderer, std::string path, int size);
    void loadFont(std::string path, int size);
    Texture* prepareText(std::string text, SDL_Color color, bool blended = false);
    Texture* prepareText(std::string text, Uint8 r, Uint8 g, Uint8 b, Uint8 a, bool blended = false);
    int textWidth(std::string text);

protected:
    TTF_Font *font = nullptr;
    SDL_Renderer *renderer = nullptr;
};


#endif //INC_8BIT_SAVEEQ_FONT_H
