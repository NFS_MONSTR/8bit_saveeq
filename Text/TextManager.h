//
// Created by nfs_monstr on 10.08.18.
//

#ifndef INC_8BIT_SAVEEQ_TEXTMANAGER_H
#define INC_8BIT_SAVEEQ_TEXTMANAGER_H

#include "FontManager.h"
#include "../Config/Config.h"

class TextManager {
public:
    TextManager(FontManager *fontManager, Config *texts);
    TextManager(FontManager *fontManager, std::string textsPath);
    ~TextManager();
    Texture* getPreparedText(std::string name);
    Texture* operator[](std::string name);
    void renderTextDirect(int x, int y, std::string font, std::string text, bool centeredX = false, bool centeredY = false);
    void renderTextDirect(int x, int y, std::string font, std::string text, SDL_Color color, bool centeredX = false, bool centeredY = false);
    int textWidth(std::string font, std::string text);

protected:
    void prepareTexts(Config *texts);
    FontManager *fontManager;
    std::map<std::string, Texture*> preparedTexts;

};


#endif //INC_8BIT_SAVEEQ_TEXTMANAGER_H
