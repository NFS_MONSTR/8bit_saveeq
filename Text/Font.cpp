//
// Created by nfs_monstr on 09.08.18.
//

#include <iostream>
#include "Font.h"

Font::Font(SDL_Renderer *renderer) {
    this->renderer = renderer;
}

Font::Font(SDL_Renderer *renderer, TTF_Font *font) {
    this->renderer = renderer;
    this->font = font;
}

Font::~Font() {
    if (font != nullptr)
        TTF_CloseFont(font);
}

Font::Font(SDL_Renderer *renderer, std::string path, int size) {
    this->renderer = renderer;
    loadFont(std::move(path), size);
}

void Font::loadFont(std::string path, int size) {
    font = TTF_OpenFont(path.c_str(), size);
    if (!font)
        std::cout << "Cant open font "<<path<< " " << TTF_GetError() << std::endl;
}

Texture* Font::prepareText(std::string text, SDL_Color color, bool blended) {
    SDL_Surface* surf =  blended? TTF_RenderUTF8_Blended(font, text.c_str(), color) : TTF_RenderUTF8_Solid(font, text.c_str(), color);
    if (!surf) {
        std::cout << "Error rendering text " << text << " " << TTF_GetError() << std::endl;
        return nullptr;
    } else {
        SDL_Texture* sdl_texture = SDL_CreateTextureFromSurface(renderer, surf);
        auto texture = new Texture(renderer, sdl_texture, surf->w, surf->h);
        SDL_FreeSurface(surf);
        return texture;
    }
}

Texture* Font::prepareText(std::string text, Uint8 r, Uint8 g, Uint8 b, Uint8 a, bool blended) {
    return prepareText(std::move(text), {r, g, b, a}, blended);
}

int Font::textWidth(std::string text) {
    int w,h;
    TTF_SizeUTF8(font, text.c_str(), &w, &h);
    return w;
}