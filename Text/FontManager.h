//
// Created by nfs_monstr on 09.08.18.
//

#ifndef INC_8BIT_SAVEEQ_FONTMANAGER_H
#define INC_8BIT_SAVEEQ_FONTMANAGER_H


#include <string>
#include <map>
#include "Font.h"

class FontManager {
public:
    explicit FontManager(SDL_Renderer *renderer);
    ~FontManager();
    void loadFont(std::string path, std::string name, int size);
    void doneFont(std::string name);

    Font* operator[](const std::string &name) {
        return fonts[name];
    }

protected:
    std::map<std::string,Font*> fonts;
    SDL_Renderer *renderer;
};


#endif //INC_8BIT_SAVEEQ_FONTMANAGER_H
