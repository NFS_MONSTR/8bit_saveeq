//
// Created by nfs_monstr on 14.08.18.
//

#include "Dragon.h"
#include "Events.h"
#include "Player.h"
#include "Level.h"

void Dragon::handleEvent(SDL_Event &e) {
    switch (e.type) {
        case SDL_USEREVENT:
            auto sender = (Entity*)e.user.data2;
            switch (e.user.code) {
                case Events::Collide:
                    if (getAction()==AnimationType::Flying && typeid(*sender)==typeid(Player) && isCollinding(sender)) {
                        if (sender->getBottomY()<=getTopY()+20) {
                            level->sendEvent(Events::EnemyKilled, sender, this);
                            setAction(AnimationType::Jumping);
                        } else {
                            level->sendEvent(Events::Attack, sender, this);
                        }
                    }
                    break;
                case Events::Attack:
                    if (getAction()==AnimationType::Flying && typeid(*sender)==typeid(Player) && isCollinding(sender)) {
                        level->sendEvent(Events::EnemyKilled, sender, this);
                        setAction(AnimationType::Jumping);
                    }
                    break;
            }
            break;
    }
}

void Dragon::tick() {
    Entity::tick();
    if (getAction() == AnimationType::Jumping)
        move(0, 15 * gravity);
    else {
        if (direction == Direction::Left)
            move(-speed, 0);
        else
            move(speed, 0);
    }

    if (getBottomX()<0)
        onFallingUnderMap();
}

void Dragon::onFallingUnderMap() {
    gravity = 0;
    level->sendEvent(Events::DestroyMe, Events::to_everyone, this);
}
