//
// Created by nfs_monstr on 03.08.18.
//

#ifndef INC_8BIT_SAVEEQ_COIN_H
#define INC_8BIT_SAVEEQ_COIN_H


#include "Entity.h"

class Coin : public Entity {
public:
    using Entity::Entity;
    void handleEvent(SDL_Event &e) override;

};


#endif //INC_8BIT_SAVEEQ_COIN_H
