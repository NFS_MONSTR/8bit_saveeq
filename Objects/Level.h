//
// Created by nfs_monstr on 27.07.18.
//

#ifndef INC_8BIT_SAVEEQ_LEVEL_H
#define INC_8BIT_SAVEEQ_LEVEL_H


#include <queue>
#include "../Textures/Texture.h"
#include "Entity.h"
#include "Events.h"
#include "../Textures/ImageManager.h"
#include "Player.h"
#include "../Text/TextManager.h"
#include "../Sound/SoundManager.h"
#include "MsgBox.h"

enum BlockType {
    Block_Air = 0,
    Block_Ground1,
    Block_Ground2,
    Block_Ground1LeftB,
    Block_Ground1RightB,
    Block_Ground2LeftB,
    Block_Ground2RightB,
    Block_Ice,
    Block_Coin, //Не сама монетка, во время обработки уровня спавнится entity монетки
    Block_Question,
    Block_Chest,
    Block_Spikes,
    Block_Dragon,//todo add good entity spawning system
    Block_HayDownLeft,
    BlocK_HayDownRight,
    Block_HayUpLeft,
    Block_HayUpRight,
    Block_HaySmallLeft,
    Block_HaySmallRight,
    Block_Player,
    Block_Tirek
};

class Level {
public:

    Level() = default;
    Level(int screen_width);
    ~Level();
    void setBackground(Texture* texture);
    void scroll(int scroll_value);
    void scrollTo(int scroll);
    void centerOn(int x);
    void setCamState(bool locked = false);
    bool getCamState();
    void run();
    void render();
    void tick();
    void handleEvent(SDL_Event &e);
    void clearEvent(SDL_Event &e);
    void setScreenWidth(int screen_width);
    void setWidth(unsigned long blocks);
    void addWidth(unsigned long blocks);
    int getMapWidth();
    int getMapHeight();
    void sendEvent(Events::EventCode code, Entity *receiver, Entity *sender);

    void addPlayer(Player *p);
    void addEntity(Entity *e);
    void addBlock(Texture *t);
    void addMsgBox(std::string name, std::string text);
    void addMsgBox(MsgBoxInfo info);
    void freeEntities();
    void placeBlock(int x, int y, int type);
    int getBlock(int x, int y);
    bool isBlockSolid(int x, int y);
    int getBlockDirect(int i, int j);
    int getBlockSize();
    Texture* getBlockTexture(int x, int y);
    bool isEntityInMap(Entity *e);
    bool loadMap(std::string file);
    void saveMap(std::string file);
    void restartLevel(bool reloadMap = false);
    void spawnEntity(int block_i, int block_j);
    Entity* getPlayer();



    void setRenderer(SDL_Renderer *renderer);
    void setRenderDebugInfo(bool render);
    void setImageManager(ImageManager *imgMgr);
    ImageManager* getImageManager();
    void setTextManager(TextManager *textManager);
    TextManager* getTextManager();
    void setSoundManager(SoundManager *soundManager);
    SoundManager* getSoundManager();

protected:
    void renderDebugInfo();
    void renderUI();

    Texture* backgroundImage = nullptr;
    int scroll_offset = 0;
    int level_offset = 0;
    int previous_level_offset = 0;
    bool camLock = false;
    int screen_width = 764;
    int max_width = 2000;
    int max_height = 720;
    const int block_width = 72;
    Player *player = nullptr;
    std::vector<Entity*> entities;
    std::vector<std::array<Uint32, 10>> levelMap;
    std::vector<Texture*> blocks;

    SDL_Renderer *renderer = nullptr;
    bool renderDbgInfo = false;

    bool stop = false;

    int currentBlock = 1;
    int currentPage = 0;

    ImageManager *imgMgr = nullptr;
    TextManager *textManager = nullptr;
    SoundManager *soundManager = nullptr;
    Uint32 startTime = 0;
    Uint32 time = 0;

    std::queue<MsgBox> msgBoxQueue;

};


#endif //INC_8BIT_SAVEEQ_LEVEL_H
