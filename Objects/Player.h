//
// Created by nfs_monstr on 29.07.18.
//

#ifndef INC_8BIT_SAVEEQ_PLAYER_H
#define INC_8BIT_SAVEEQ_PLAYER_H


#include "Entity.h"

class Player : public Entity {
public:
    using Entity::Entity;

    void tick() override;
    void handleEvent(SDL_Event &e) override;
    void nextAnimFrame() override;

    void onFallingUnderMap() override;
    void onDeath() override;

    void fly();
    void fallAttack();
    void preparePowerUp();
    void powerUp();
    void frontAttack();

    int getCoinCount();
    int getScore();

protected:
    int coint_count = 0;
    int score = 0;
    bool isFlying = false;
    bool isFallAttacking = false;
    bool isPreparePowerUp = false;
    int preparePowerUpCounter = 0;
    int preparePowerUpFrames = 8;
    bool isPowerUp = false;
    int powerUpStartTime = 0;
    int powerUpMax = 3740;//5840 - value from video
    bool isFrontAttack = false;
    int frontAttackCount = 0;
    int frontAttackMax = 1;
    int fallAttackCount = 0;
    int fallAttackMax = 1;
    int flyTimeMax = 1040;//4 блока пролететь
    int flyTimeStarted = 0;
    int flyCount = 0;
    int flyCountMax = 1;
    int gravityDefault = 1;

    bool jumpHold = false;
};


#endif //INC_8BIT_SAVEEQ_PLAYER_H
