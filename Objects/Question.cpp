//
// Created by nfs_monstr on 07.08.18.
//

#include "Question.h"
#include "Events.h"
#include "Player.h"
#include "Level.h"
#include "FakeCoin.h"

Question::Question() {
    setTickAnimation(false);
}

Question::Question(Animations *animation) : Entity(animation) {
    setTickAnimation(false);
}

void Question::handleEvent(SDL_Event &e) {
    Entity::handleEvent(e);
    switch (e.type) {
        case SDL_USEREVENT:
            switch (e.user.code) {
                case Events::Collide:
                    auto sender = (Entity*)e.user.data2;
                    if (e.user.data1 == nullptr && animationOffset==0 && typeid(*sender)==typeid(Player) && isCollinding(sender) &&
                            sender->getTopY()+gravity>=getBottomY()) {
                        animationOffset++;
                        jump();
                        level->sendEvent(Events::CoinCollected,sender,this);

                        auto c = new FakeCoin(level->getImageManager()->getAnimation("coin"));
                        c->setPos(x,y-level->getBlockSize());
                        level->addEntity(c);
                        c->jump();
                    }
                    break;
            }
            break;
    }
}

bool Question::onGround() {
    return level != nullptr && level->getBlock(x, getTopY())==Block_Question;
}

void Question::jump() {
    if (!isJumping && onGround()) {
        isJumping = true;
        velocity = max_velocity;
    }
}

void Question::tick() {
    int offsetBottom = getCurrentFrame()->getBottomY(direction)-getCurrentFrame()->getCenterY(direction);
    int offsetTop = getCurrentFrame()->getTopY(direction) - getCurrentFrame()->getCenterY(direction);

    if (isJumping) {
        int offset = 0;
        velocity>=0 ? offset = offsetBottom : offset = offsetTop;
        if (!level->isBlockSolid(x, y + offset + velocity) || level->getBlock(x, y + offset + velocity)==Block_Question || level->getBlock(x, y + velocity)==Block_Question)
            move(0, velocity);
        else {
            if (velocity>=0)// Чтобы заканчивать падение плавно
                move(0, getDistanceFromNextBlock(Direction::Down));
            else {
                int dist = getDistanceFromNextBlock(Direction::Up);
                if (dist>=0)
                    velocity = -gravity;
                move(0, dist);
            }
        }
        velocity += gravity;
        if (level->getBlock(x,getTopY())==Block_Question && velocity>=0) {
            velocity = 0;
            isJumping = false;
        }
    }
    Entity::tick();
}