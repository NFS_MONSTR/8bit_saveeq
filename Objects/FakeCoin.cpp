//
// Created by nfs_monstr on 07.08.18.
//

#include "FakeCoin.h"
#include "Level.h"

void FakeCoin::jump() {
    if (!isJumping) {
        if (jump_number<jumps_count) {
            isJumping = true;
            velocity = max_vel[jump_number];
            jump_number++;
        } else {
            level->sendEvent(Events::DestroyMe, Events::to_everyone, this);//эта монетка сразу исчезает
            jump_number = 0;
        }
    }
}

void FakeCoin::tick() {
    int offsetBottom = getCurrentFrame()->getBottomY(direction)-getCurrentFrame()->getCenterY(direction);
    int offsetTop = getCurrentFrame()->getTopY(direction) - getCurrentFrame()->getCenterY(direction);

    if (isJumping) {
        int offset = 0;
        velocity>=0 ? offset = offsetBottom : offset = offsetTop;
        if (!level->isBlockSolid(x, y + offset + velocity))
            move(0, velocity);
        else {
            if (velocity>=0)// Чтобы заканчивать падение плавно
                move(0, getDistanceFromNextBlock(Direction::Down));
            else {
                int dist = getDistanceFromNextBlock(Direction::Up);
                if (dist>=0)
                    velocity = -gravity;
                move(0, dist);
            }
        }
        velocity += gravity;
        if (onGround()) {
            velocity = 0;
            isJumping = false;
            jump();
        }
    }
    Entity::tick();
}