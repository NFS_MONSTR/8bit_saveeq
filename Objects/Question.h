//
// Created by nfs_monstr on 07.08.18.
//

#ifndef INC_8BIT_SAVEEQ_QUESTION_H
#define INC_8BIT_SAVEEQ_QUESTION_H


#include "Entity.h"

class Question : public Entity {
public:
    Question();
    explicit Question(Animations *animation);
    void handleEvent(SDL_Event &e) override;
    bool onGround() override;
    void jump() override;
    void tick() override;

protected:
    int max_velocity = -10;

};


#endif //INC_8BIT_SAVEEQ_QUESTION_H
