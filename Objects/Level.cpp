//
// Created by nfs_monstr on 27.07.18.
//
#include <fstream>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include "Level.h"
#include "Coin.h"
#include "Player.h"
#include "Chest.h"
#include "Spikes.h"
#include "Question.h"
#include "Dragon.h"
#include "Tirek.h"

Level::Level(int screen_width) {
    this->screen_width = screen_width;
}

Level::~Level() {
    freeEntities();
    for (const auto& item : blocks)
        delete item;
}

void Level::setBackground(Texture *texture) {
    backgroundImage = texture;
}

void Level::scroll(int scroll_value) {
    previous_level_offset = level_offset;
    //check level scroll borders
    if (level_offset+scroll_value>=0 && level_offset+scroll_value+screen_width<=max_width) {
        //check that scrolling background is fine
        if ((scroll_offset + scroll_value + screen_width >= backgroundImage->getWidth()) || (scroll_offset+scroll_value<0))
            scroll_offset = (scroll_offset + backgroundImage->getCycleWidth()) % backgroundImage->getWidth();
        scroll_offset += scroll_value;
        level_offset += scroll_value;
    } else {
        if (level_offset+scroll_value<0) {
            level_offset = 0;
            scroll_offset = 0;
        } else {
            if (level_offset+scroll_value+screen_width>max_width) {
                level_offset = max_width-screen_width;
            }
        }
    }
}

void Level::scrollTo(int scroll) {
    if (!camLock)
        this->scroll(scroll-level_offset);
}

void Level::centerOn(int x) {
    scrollTo(x-screen_width / 2);
}

void Level::setCamState(bool locked) {
    camLock = locked;
}

bool Level::getCamState() {
    return camLock;
}

void Level::run() {
    startTime = SDL_GetTicks();
    while (!stop) {

        render();

        tick();

        SDL_RenderPresent(renderer);
        SDL_Delay(20);
    }
}

void Level::render() {
    if (backgroundImage!= nullptr)
        backgroundImage->render(-scroll_offset,0);

    for (int i = level_offset/block_width; i < std::min((size_t)(level_offset+screen_width)/block_width+1,levelMap.size()); ++i)
        for (int j = 0; j < 10; ++j) {
            if (levelMap[i][j]!=0) {
                blocks[levelMap[i][j]]->render(-level_offset + i*block_width - blocks[levelMap[i][j]]->getTopX(Direction::Right), j*block_width - blocks[levelMap[i][j]]->getTopY(Direction::Right));
            }
        }

    for (auto entity : entities) {
        if (entity!=player)
            entity->renderWithOffset(-level_offset, 0);
    }

    if (player!= nullptr) {
        scrollTo(player->getX()-screen_width / 2);
        player->renderWithOffset(-level_offset, 0);
    }

    if (!msgBoxQueue.empty())
        msgBoxQueue.front().render();

    renderUI();

    if (renderDbgInfo)
        renderDebugInfo();
}

void Level::tick() {
    SDL_Event e;
    if (msgBoxQueue.empty()) {
        while (SDL_PollEvent(&e)) {
            handleEvent(e);
            for (const auto &entity : entities)
                if (isEntityInMap(entity))
                    entity->handleEvent(e);
        }

        for (const auto &entity : entities)
            if (isEntityInMap(entity))
                entity->tick();

        time = SDL_GetTicks() - startTime;
    } else {
        while (SDL_PollEvent(&e))
            handleEvent(e);
        if (!msgBoxQueue.empty()) {
            msgBoxQueue.front().tick();
            if (msgBoxQueue.front().isDone()) {
                //fix level timing
                startTime+=SDL_GetTicks()-msgBoxQueue.front().getStartTime();
                msgBoxQueue.pop();
            }
        }
    }
}

void Level::handleEvent(SDL_Event &e) {
    switch (e.type) {
        case SDL_QUIT:
            stop = true;
            clearEvent(e);
            break;
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
                case SDLK_ESCAPE:
                    stop = true;
                    clearEvent(e);
                    break;
                case SDLK_s:
                    saveMap("Resources/map.bin");
                    clearEvent(e);
                    break;
                case SDLK_e:
                    addWidth(1);
                    clearEvent(e);
                    break;
                case SDLK_d:
                    player->setSpeed(player->getSpeed()+10);
                    clearEvent(e);
                    break;
                case SDLK_f:
                    player->setSpeed(player->getSpeed()-10);
                    clearEvent(e);
                    break;
                case SDLK_q:
                    currentPage = 1-currentPage;
                    break;
                case SDLK_1:
                case SDLK_2:
                case SDLK_3:
                case SDLK_4:
                case SDLK_5:
                case SDLK_6:
                case SDLK_7:
                case SDLK_8:
                case SDLK_9:
                    currentBlock = e.key.keysym.sym-SDLK_0 + 12*currentPage;
                    clearEvent(e);
                    break;
                case SDLK_0:
                    currentBlock = 10 + 12*currentPage;
                    clearEvent(e);
                    break;
                case SDLK_MINUS:
                    currentBlock = 11 + 12*currentPage;
                    clearEvent(e);
                    break;
                case SDLK_EQUALS:
                    currentBlock = 12 + 12*currentPage;
                    clearEvent(e);
                    break;
                case SDLK_r:
                    getPlayer()->setPos(100,500);
                    clearEvent(e);
                    break;
                case SDLK_F1:
                    setRenderDebugInfo(false);
                    clearEvent(e);
                    break;
                case SDLK_F2:
                    setRenderDebugInfo(true);
                    clearEvent(e);
                    break;
                case SDLK_F3:
                    addMsgBox("tirek","tirek_0\ntirek_1\ntirek_2");
                    clearEvent(e);
                    break;
                case SDLK_F4:
                    setCamState(!getCamState());
                    clearEvent(e);
                    break;
                case SDLK_RETURN:
                case SDLK_RETURN2:
                    if (!msgBoxQueue.empty()) {//todo refactor
                        //fix level timing
                        startTime+=SDL_GetTicks()-msgBoxQueue.front().getStartTime();
                        msgBoxQueue.pop();

                    }
                    clearEvent(e);
                    break;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            switch (e.button.button){
                case SDL_BUTTON_LEFT:
                    placeBlock(e.button.x, e.button.y, currentBlock);
                    clearEvent(e);
                    break;
                case SDL_BUTTON_RIGHT:
                    placeBlock(e.button.x, e.button.y, 0);
                    clearEvent(e);
                    break;
            }
            break;
        case SDL_USEREVENT:
            if (e.user.data1 == nullptr)
                switch (e.user.code) {
                    case Events::DestroyMe:
                        delete ((Entity *)e.user.data2);
                        entities.erase(std::remove(entities.begin(),entities.end(), (Entity *)e.user.data2), entities.end());
                        clearEvent(e);
                        break;
                    case Events::RestartLevel:
                        restartLevel();
                        clearEvent(e);
                        break;
                    case Events::CreateMsgBox:
                        if (e.user.data2!=nullptr) {
                            auto *info = (MsgBoxInfo *)e.user.data2;
                            addMsgBox(*info);
                            delete info;
                        }
                        clearEvent(e);
                        break;
            }
            break;
    }
}

void Level::clearEvent(SDL_Event &e) {
    e.type = SDL_FIRSTEVENT;
}

void Level::setScreenWidth(int screen_width) {
    this->screen_width = screen_width;
}

void Level::setWidth(unsigned long blocks) {
    levelMap.resize(blocks);
    max_width = blocks*block_width;
}

void Level::addWidth(unsigned long blocks) {
    setWidth(levelMap.size()+blocks);
}

int Level::getMapWidth() {
    return max_width;
}

int Level::getMapHeight() {
    return max_height;
}

void Level::sendEvent(Events::EventCode code, Entity *receiver, Entity *sender) {
    SDL_Event e;
    e.type = SDL_USEREVENT;
    e.user.code = code;
    e.user.data1 = receiver;
    e.user.data2 = sender;
    SDL_PushEvent(&e);
}

void Level::addPlayer(Player *p) {
    player = p;
    addEntity(p);
}

void Level::addEntity(Entity *e) {
    entities.push_back(e);
    e->setLevel(this);
}

void Level::addBlock(Texture *t) {
    blocks.push_back(t);
}

void Level::addMsgBox(std::string name, std::string text) {
    msgBoxQueue.push(MsgBox(renderer, textManager, std::move(name), std::move(text), -1, 64));
}

void Level::addMsgBox(MsgBoxInfo info) {
    msgBoxQueue.push(MsgBox(renderer, textManager, std::move(info), -1, 64));
}

void Level::freeEntities() {
    for (const auto &entity : entities)
        delete entity;
    entities.resize(0);
}

void Level::placeBlock(int x, int y, int type) {
    int i = (level_offset+x)/block_width;
    int j = y/block_width;
    if (i<levelMap.size()) {
        levelMap[i][j] = type;
    }
}

int Level::getBlock(int x, int y) {
    int i = x/block_width;
    int j = y/block_width;
    if (i>=0 && i<levelMap.size() && j>=0 && j<10) {
        return levelMap[i][j];
    } else {
        std::cout << "[Warning] getting block out of range " << x << " " << y << std::endl;
        return 0;
    }
}

bool Level::isBlockSolid(int x, int y) {
    return blocks[getBlock(x,y)]->isSolid();
}

int Level::getBlockDirect(int i, int j) {
    if (i<levelMap.size()) {
        return levelMap[i][j];
    } else {
        std::cout << "[Warning] getting direct block out of range " << i << " " << j << std::endl;
        return 0;
    }
}

int Level::getBlockSize() {
    return block_width;
}

Texture* Level::getBlockTexture(int x, int y) {
    return blocks[getBlock(x,y)];
}

bool Level::isEntityInMap(Entity *e) {
    constexpr int offset = 100;
    return e->isColliding(level_offset-offset, -offset, level_offset + screen_width + offset, max_height + offset);
}

bool Level::loadMap(std::string file) {
    SDL_RWops* input = SDL_RWFromFile(file.c_str(), "r+b");
    if (input != nullptr) {
        Uint64 size;
        SDL_RWread(input, &size, sizeof(size), 1);
        setWidth(size);
        for (int i = 0; i < levelMap.size(); ++i) {
            for (int j = 0; j < levelMap[i].size(); ++j) {
                SDL_RWread(input, &levelMap[i][j], sizeof(levelMap[i][j]), 1);
                spawnEntity(i, j);//spawns entity if needed
            }
        }
        SDL_RWclose(input);
        return true;
    } else {
        std::cout << "[Warning] Map file " << file << " doesn't exist" << std::endl;
        return false;
    }
}

void Level::saveMap(std::string file) {
    SDL_RWops* output = SDL_RWFromFile(file.c_str(), "w+b");
    Uint64 sz = levelMap.size();
    SDL_RWwrite(output, &sz, sizeof(sz), 1);
    for (const auto& item:levelMap) {
        SDL_RWwrite(output, &item, sizeof(item), 1);
    }
    SDL_RWclose(output);
}

void Level::restartLevel(bool reloadMap) {
    freeEntities();
    if (reloadMap) {
        /*todo reloadMap*/
        ;
    }
    else {
        for (int i = 0; i < levelMap.size(); ++i) 
            for (int j = 0; j < levelMap[i].size(); ++j)
                spawnEntity(i, j);//spawns entity if needed
    }

    startTime = SDL_GetTicks();
}

void Level::spawnEntity(int block_i, int block_j) {
    Entity *e;
    switch (levelMap[block_i][block_j]) {
        case Block_Question:
            e = new Question(imgMgr->getAnimation("question"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            addEntity(e);
            break;
        case Block_Coin:
            e = new Coin(imgMgr->getAnimation("coin"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            addEntity(e);
            break;
        case Block_Chest:
            e = new Chest(imgMgr->getAnimation("chest"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            addEntity(e);
            break;
        case Block_Spikes:
            e = new Spikes(imgMgr->getAnimation("spikes"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            addEntity(e);
            break;
        case Block_Dragon:
            e = new Dragon(imgMgr->getAnimation("dragon"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            e->setAction(AnimationType::Flying);
            e->setDirection(Direction::Left);
            addEntity(e);
            break;
        case Block_Player:
            e = new Player(imgMgr->getAnimation("player_4"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width/2);
            addPlayer((Player*)e);
            break;
        case Block_Tirek:
            e = new Tirek(imgMgr->getAnimation("tirek"));
            e->setPos(block_i*block_width+block_width/2, block_j*block_width + block_width - (e->getCurrentFrame()->getBottomY(Direction::Current)-e->getCurrentFrame()->getCenterY(Direction::Current)));
            e->setDirection(Direction::Left);
            addEntity(e);
            break;
    }
}

Entity* Level::getPlayer() {
    return player;
}

void Level::setRenderer(SDL_Renderer *renderer) {
    this->renderer = renderer;
}

void Level::setRenderDebugInfo(bool render) {
    renderDbgInfo = render;
}

void Level::setImageManager(ImageManager *imgMgr) {
    this->imgMgr = imgMgr;
}

ImageManager* Level::getImageManager() {
    return imgMgr;
}

void Level::setTextManager(TextManager *textManager) {
    this->textManager = textManager;
}

TextManager* Level::getTextManager() {
    return textManager;
}

void Level::setSoundManager(SoundManager *soundManager) {
    this->soundManager = soundManager;
}

SoundManager* Level::getSoundManager() {
    return soundManager;
}

void Level::renderDebugInfo() {
    if (renderer != nullptr) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (int i = 0; i < 10; i++)//render lines
            SDL_RenderDrawLine(renderer, 0, i * block_width, screen_width, i * block_width);
        for (int j = previous_level_offset/block_width; j < std::min((size_t)(previous_level_offset+screen_width)/block_width+1,levelMap.size()); ++j)
            SDL_RenderDrawLine(renderer, -previous_level_offset + j*block_width, 0, -previous_level_offset + j*block_width, max_height);

        const std::vector<int> hidden_blocks = {Block_Coin, Block_Question, Block_Chest, Block_Spikes, Block_Dragon, Block_Player, Block_Tirek};
        for (int i = previous_level_offset/block_width; i < std::min((size_t)(previous_level_offset+screen_width)/block_width+1,levelMap.size()); ++i)//render hidden blocks
            for (int j = 0; j < 10; ++j)
                if (std::find(hidden_blocks.begin(), hidden_blocks.end(), levelMap[i][j])!=hidden_blocks.end())
                    textManager->renderTextDirect(-previous_level_offset + i*block_width + block_width/2, j*block_width + block_width/2, "gui_2", std::to_string(levelMap[i][j]), true, true);

        SDL_Rect rect;
        for (auto entity : entities) {

            /*
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);//Render image borders
            rect.x = entity->getX() - entity->getCurrentFrame()->getCenterX(entity->getDirection()) -level_offset;
            rect.y = entity->getY() - entity->getCurrentFrame()->getCenterY(entity->getDirection());
            rect.w = entity->getCurrentFrame()->getWidth();
            rect.h = entity->getCurrentFrame()->getHeight();
            SDL_RenderDrawRect(renderer, &rect);
             */

            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);//Render hitbox
            rect.x = entity->getTopX() -previous_level_offset;
            rect.y = entity->getTopY();
            rect.w = entity->getCurrentFrame()->getBottomX(entity->getDirection()) - entity->getCurrentFrame()->getTopX(entity->getDirection());//convert BottomX and BottomY to width and height
            rect.h = entity->getCurrentFrame()->getBottomY(entity->getDirection()) - entity->getCurrentFrame()->getTopY(entity->getDirection());
            SDL_RenderDrawRect(renderer, &rect);


            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);//Render stand line
            rect.x = entity->getStandLeft() - previous_level_offset;
            rect.y = entity->getBottomY();
            rect.w = entity->getStandRight() - previous_level_offset;
            SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.w, rect.y);


            SDL_RenderDrawPoint(renderer, entity->getX()-previous_level_offset, entity->getY());//RenderCenter
        }
    }
}

void Level::renderUI() {//todo refactor ui subsystem
    if (textManager != nullptr) {
        textManager->getPreparedText("player_rd")->render(10, 10);
        std::stringstream scoreStream;
        scoreStream << std::setfill('0') << std::setw(6) << player->getScore();
        textManager->renderTextDirect(10, 35, "gui_2", scoreStream.str());

        imgMgr->getAnimation("coin")->getAnimation(AnimationType::Standing, 0)->renderCentered(screen_width/2-39, 33);
        textManager->renderTextDirect(screen_width/2, 33, "gui_2", "x", true, true);
        textManager->renderTextDirect(screen_width/2+10, 33, "gui_2", std::to_string(player->getCoinCount()), false, true);

        textManager->getPreparedText("world")->renderCentered(
                screen_width - textManager->getPreparedText("world")->getWidth() / 2 -
                textManager->getPreparedText("time")->getWidth() - 70,
                10 + textManager->getPreparedText("world")->getHeight() / 2);

        textManager->renderTextDirect(
                screen_width - textManager->getPreparedText("world")->getWidth() / 2 -
                textManager->getPreparedText("time")->getWidth() - 70,
                35, "gui_2", "1-1", true);


        textManager->getPreparedText("time")->renderCentered(
                screen_width - textManager->getPreparedText("time")->getWidth() / 2 - 10,
                10 + textManager->getPreparedText("time")->getHeight() / 2);

        std::string timeStr;
        timeStr.resize(10);
        snprintf((char *) timeStr.data(), 10, "%02d:%02d", time / 1000 / 60, time / 1000 % 60);
        textManager->renderTextDirect(screen_width - textManager->getPreparedText("time")->getWidth() / 2 - 10, 35,
                                      "gui_2", timeStr, true);

    }
}