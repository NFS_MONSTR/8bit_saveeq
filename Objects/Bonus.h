//
// Created by nfs_monstr on 07.08.18.
//

#ifndef INC_8BIT_SAVEEQ_BONUS_H
#define INC_8BIT_SAVEEQ_BONUS_H


#include "Entity.h"
#include "../build_defs.h"

class Bonus : public Entity {
public:
    using Entity::Entity;

    void tick() override;
    void jump() override;
    void handleEvent(SDL_Event &e) override;

protected:
    const float angle = 1.39626;
    int speed = 20;
    int velocity_x = 0;
};


#endif //INC_8BIT_SAVEEQ_BONUS_H
