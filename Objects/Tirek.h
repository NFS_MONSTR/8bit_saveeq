//
// Created by nfs_monstr on 20.08.18.
//

#ifndef INC_8BIT_SAVEEQ_TIREK_H
#define INC_8BIT_SAVEEQ_TIREK_H


#include "Entity.h"

class Tirek : public Entity {
public:

    explicit Tirek(Animations *animations);

    void tick() override;
    void go(Direction d = Direction::Current) override;
    void handleEvent(SDL_Event &e) override;

    void ai();

protected:
    int offset = 0;
    Direction move_dir = Direction::Left;
    int wait_timer = 200;
    int start_time = 0;
    int health = 10;
    int attack_cooldown_max = 30;
    int attack_cooldown = 0;
};


#endif //INC_8BIT_SAVEEQ_TIREK_H
