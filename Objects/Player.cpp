//
// Created by nfs_monstr on 29.07.18.
//
#include <typeinfo>
#include "Player.h"
#include "Level.h"
#include "Dragon.h"

void Player::tick() {
    const Uint8 *keys = SDL_GetKeyboardState(nullptr);//Нельзя использовать SDL_KEYDOWN т.к. https://stackoverflow.com/questions/29373203/sdl-2-0-key-repeat-and-delay
    if (keys[SDL_SCANCODE_RIGHT] && getAction()!=AnimationType::Prepare_PowerUp && getAction()!=AnimationType::PowerUp) {
        if (getAction()!=AnimationType::Jumping && getAction()!=AnimationType::Flying && getAction()!=AnimationType::Attacking)
            setAction(AnimationType::Walking);
        setDirection(Direction::Right);
        go();
    } else if (getAction()==AnimationType::Walking && getDirection()==Direction::Right) {
        setAction(AnimationType::Standing);
    }

    if (keys[SDL_SCANCODE_LEFT] && getAction()!=AnimationType::Prepare_PowerUp && getAction()!=AnimationType::PowerUp) {
        if (getAction()!=AnimationType::Jumping && getAction()!=AnimationType::Flying && getAction()!=AnimationType::Attacking)
            setAction(AnimationType::Walking);
        setDirection(Direction::Left);
        go();
    } else if (getAction()==AnimationType::Walking && getDirection()==Direction::Left) {
        setAction(AnimationType::Standing);
    }

    if (keys[SDL_SCANCODE_UP]) {
        if (getAction()==AnimationType::PowerUp)
            goUpDown(Direction::Up);
    }

    if (keys[SDL_SCANCODE_DOWN]) {
        if (getAction()==AnimationType::PowerUp)
            goUpDown(Direction::Down);
    }

    if (keys[SDL_SCANCODE_SPACE]) {
        if (!isJumping && !isFallAttacking && !isFlying && !isPreparePowerUp && !isPowerUp && onGround() && !jumpHold) {
            jump();
            jumpHold = true;
        }
        else
            if (!jumpHold) {
                fallAttack();
                jumpHold = true;
            }
    } else {
        jumpHold = false;
    }

    if (keys[SDL_SCANCODE_Z]) {
        fly();
    }

    if (keys[SDL_SCANCODE_X]) {
        frontAttack();
    }

    if (isPowerUp || isFrontAttack)
        level->sendEvent(Events::Attack, Events::to_everyone, this);
    level->sendEvent(Events::Collide, Events::to_everyone, this);


    int offsetBottom = getCurrentFrame()->getBottomY(direction)-getCurrentFrame()->getCenterY(direction);
    int offsetTop = getCurrentFrame()->getTopY(direction) - getCurrentFrame()->getCenterY(direction);
    int xOffLeft = getCurrentFrame()->getTopX(direction)-getCurrentFrame()->getCenterX(direction);
    int xOffRight = getCurrentFrame()->getBottomX(direction)-getCurrentFrame()->getCenterX(direction);


    if (isJumping) {
        int offset = 0;
        velocity>=0 ? offset = offsetBottom : offset = offsetTop;
        if (!level->isBlockSolid(x, y + offset + velocity) && !level->isBlockSolid(x + xOffLeft, y + offset + velocity) && !level->isBlockSolid(x + xOffRight, y + offset + velocity))
            move(0, velocity);
        else {
            if (velocity>=0)// Чтобы заканчивать падение плавно
                move(0, std::min(getDistanceFromNextBlock(Direction::Down),velocity));
            else {
                int dist = getDistanceFromNextBlock(Direction::Up);
                if (dist>=0)
                    velocity = -gravity;
                move(0, dist);
            }
        }
        velocity += gravity;
        if (onGround()) {
            velocity = 0;
            isJumping = false;
            setAction(AnimationType::Standing);
            flyCount = 0;
        }
    } else if (isFlying) {
        if (SDL_GetTicks()-flyTimeStarted>flyTimeMax) {
            setAction(AnimationType::Standing);
            isFlying = false;
        }
    } else if (isPreparePowerUp) {
        if (preparePowerUpCounter>preparePowerUpFrames) {
            powerUp();
        }
    } else if (isPowerUp) {
        go();
        if (SDL_GetTicks()-powerUpStartTime>powerUpMax) {
            isPowerUp = false;
            setAction(AnimationType::Standing);
        }

    } else {
        if (!onGround()) {
            if (!level->isBlockSolid(x, y + offsetBottom + 10*gravity) && !level->isBlockSolid(getStandLeft(), y + offsetBottom + 10*gravity) && !level->isBlockSolid(getStandRight(), y + offsetBottom + 10*gravity))
                move(0, 10*gravity);
            else
                move(0, std::min(getDistanceFromNextBlock(Direction::Down),10*gravity));// Чтобы заканчивать падение плавно
        } else {
            flyCount = 0;
            if (isFallAttacking) {
                setAction(AnimationType::Standing);
                isFallAttacking = false;
                gravity = gravityDefault;
                fallAttackCount = 0;
            } else if (isFrontAttack) {
                setAction(AnimationType::Standing);
                isFrontAttack = false;
                frontAttackCount = 0;
            }
        }
    }

    Entity::tick();
}

void Player::handleEvent(SDL_Event &e) {
    Entity::handleEvent(e);
    switch (e.type) {
        case SDL_USEREVENT:
            if (e.user.data1==this) {
                auto sender = (Entity*) e.user.data2;
                switch (e.user.code) {
                    case Events::CoinCollected:
                        coint_count += 1;
                        level->getSoundManager()->playSound("coin");
                        e.type = SDL_FIRSTEVENT;
                        break;
                    /*case Events::Collide:
                        int xOffset = x > sender->getX() ? sender->getBottomX() - getTopX() : sender->getTopX() - getBottomX();
                        int yOffset = y > sender->getY() ? getTopY() - sender->getBottomY() : sender->getTopY() - getBottomY();
                        x += xOffset;
                        y += yOffset;
                        e.type = SDL_FIRSTEVENT;
                        break;*/
                    case Events::EnemyKilled:
                        if (typeid(*sender)== typeid(Dragon)) {
                            score+=50;
                            e.type = SDL_FIRSTEVENT;
                        }
                        break;
                    case Events::PowerUpPicked:
                        preparePowerUp();
                        level->getSoundManager()->playSound("powerup");
                        e.type = SDL_FIRSTEVENT;
                        break;
                    case Events::Attack:
                        if (getAction()!=AnimationType::PowerUp && getAction()!=AnimationType::Prepare_PowerUp) {
                            onDeath();
                        }
                        e.type = SDL_FIRSTEVENT;
                        break;
                }
            }
            break;
    }
}

void Player::nextAnimFrame() {
    int off = animationOffset;
    Entity::nextAnimFrame();
    if (animationOffset!=off && getAction()==AnimationType::Prepare_PowerUp)
        preparePowerUpCounter++;
}
void Player::onFallingUnderMap() {
    onDeath();
}

void Player::onDeath() {
    level->sendEvent(Events::RestartLevel, Events::to_everyone, this);
}

int Player::getCoinCount() {
    return coint_count;
}

int Player::getScore() {
    return score;
}

void Player::fly() {
    if (!onGround() && !isFlying && !isPreparePowerUp && !isFrontAttack && !isPowerUp && flyCount<flyCountMax) {
        velocity = 0;
        isJumping = false;
        setAction(AnimationType::Flying);
        isFlying = true;
        flyTimeStarted = SDL_GetTicks();
        flyCount++;
    }
}

void Player::fallAttack() {
    if (!onGround() && !isPreparePowerUp && !isPowerUp && !isFrontAttack && fallAttackCount < fallAttackMax) {
        velocity = 0;
        isJumping = false;
        isFlying = false;
        isPowerUp = false;
        isPreparePowerUp = false;
        setAction(AnimationType::Jumping);
        animationOffset = 2;
        isFallAttacking = true;
        gravity *=2;
        fallAttackCount++;
    }
}

void Player::preparePowerUp() {
    if (!isPreparePowerUp) {
        setAction(AnimationType::Prepare_PowerUp);
        setDirection(Direction::Right);
        isJumping = false;
        isPreparePowerUp = true;
        isPowerUp = false;
        isFlying = false;
        if (isFallAttacking)
            gravity = gravityDefault;
        isFallAttacking = false;
        preparePowerUpCounter = 0;
    }
}

void Player::powerUp() {
    if (isPreparePowerUp) {
        setAction(AnimationType::PowerUp);
        isPreparePowerUp = false;
        isPowerUp = true;
        powerUpStartTime = SDL_GetTicks();
    }
}

void Player::frontAttack() {
    if (!onGround() && !isPreparePowerUp && !isPowerUp && !isFallAttacking && frontAttackCount < frontAttackMax) {
        velocity = 0;
        isJumping = false;
        isFlying = false;
        isPowerUp = false;
        isPreparePowerUp = false;
        isFallAttacking = false;
        setAction(AnimationType::Attacking);
        isFrontAttack = true;
        frontAttackCount++;
    }
}
