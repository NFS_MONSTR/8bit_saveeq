//
// Created by nfs_monstr on 07.08.18.
//

#ifndef INC_8BIT_SAVEEQ_SPIKES_H
#define INC_8BIT_SAVEEQ_SPIKES_H


#include "Entity.h"

class Spikes : public Entity {
public:
    using Entity::Entity;

    void handleEvent(SDL_Event &e) override;
};


#endif //INC_8BIT_SAVEEQ_SPIKES_H
