//
// Created by nfs_monstr on 07.08.18.
//

#ifndef INC_8BIT_SAVEEQ_FAKECOIN_H
#define INC_8BIT_SAVEEQ_FAKECOIN_H


#include "Entity.h"

class FakeCoin : public Entity {
public:
    using Entity::Entity;

    void jump() override;
    void tick() override;

protected:
    int max_vel[3] = {-18, -16, -12};
    int jumps_count = 3;
    int jump_number = 0;

};


#endif //INC_8BIT_SAVEEQ_FAKECOIN_H
