//
// Created by nfs_monstr on 27.07.18.
//

#include <iostream>
#include "Entity.h"
#include "Level.h"

Entity::Entity(Animations *animation) {
    setTextures(animation);
}

void Entity::setTextures(Animations *animation) {
    animations = animation;
}

void Entity::render() {
    render(x,y);
}

void Entity::render(int x, int y) {
    switch (direction) {
        case Direction::Left:
            getCurrentFrame()->renderCentered(x,y, nullptr, 0.0, SDL_FLIP_HORIZONTAL);
            break;
        case Direction::Right:
            getCurrentFrame()->renderCentered(x,y);
            break;
    }
}

void Entity::renderWithOffset(int offX, int offY) {
    render(x + offX, y + offY);
}

void Entity::tick() {

    if (getTopY()>level->getMapHeight())
        onFallingUnderMap();

    if (tickAnimation && SDL_GetTicks()-lastAnimChange>=animations->getDelay(currentAction))
        nextAnimFrame();
}

void Entity::handleEvent(SDL_Event &e) {

}

void Entity::setPos(int x, int y) {
    this->x = x;
    this->y = y;
}

void Entity::setSpeed(int speed) {
    this->speed = speed;
}

int Entity::getSpeed() {
    return speed;
}

void Entity::move(int x, int y) {
    this->x += x;
    this->y += y;
}

void Entity::jump() {
    if (!isJumping && onGround()) {
        isJumping = true;
        velocity = max_velocity;
        setAction(AnimationType::Jumping);
    }
}

void Entity::go(Direction d) {
    if (d==Direction::Current)
        d = direction;
    if (d==Direction::Right)
        if (!level->isBlockSolid(getBottomX() + speed, getTopY()) &&
                !isCollide(getBottomX() + speed, y, 3) &&
                !isCollide(getBottomX() + speed, getBottomY(), 4) &&// 4 чтобы не цепляться за пол
                getBottomX() + speed < level->getMapWidth())
            move(speed, 0);
        else
            move(getDistanceFromNextBlock(d),0);
    else
        if (!level->isBlockSolid(getTopX() - speed, getTopY()) &&
                !isCollide(getTopX() - speed, y, 3) &&
                !isCollide(getTopX() - speed, getBottomY(), 4) &&
                getTopX() - speed>0)
            move(-speed, 0);
        else
            move(getDistanceFromNextBlock(d),0);
}

void Entity::goUpDown(Direction d) {
    switch (d) {
        case Direction::Up:
            if (!level->isBlockSolid(x, getTopY() - speed) &&
                !level->isBlockSolid(getStandLeft(), getTopY() - speed) &&
                !level->isBlockSolid(getStandRight(), getTopY() - speed) &&
                getTopY() - speed > 0)
                move(0, -speed);
            else
                move(0,getDistanceFromNextBlock(d));
            break;
        case Direction::Down:
            if (!level->isBlockSolid(x, getBottomY() + speed) &&
                !level->isBlockSolid(getStandLeft(), getBottomY() + speed) &&
                !level->isBlockSolid(getStandRight(), getBottomY() + speed) &&
                getBottomY() + speed < level->getMapHeight())
                move(0, speed);
            break;
        default:
            std::cout << "[Warning]Direction is unsupported" << std::endl;
            break;
    }
}

bool Entity::onGround() {
    return level != nullptr && (isCollide(getStandLeft(), getBottomY()) || isCollide(getStandRight(), getBottomY()) || isCollide(x, getBottomY()));
}

int Entity::getDistanceFromNextBlock(Direction d) {
    if (level != nullptr) {
        int offset;
        int j;
        int dist;
        switch (d) {
            case Direction::Down: {
                offset = getCurrentFrame()->getBottomY(d) - getCurrentFrame()->getCenterY(d);
                j = (y + offset) / level->getBlockSize();
                int x_left = getStandLeft();
                int x_right = getStandRight();
                int y_off = std::min(level->getBlockTexture(x_left, y + offset)->getTopY(Direction::Current),level->getBlockTexture(x_right, y + offset)->getTopY(Direction::Current));

                if (y_off < 0)
                    return j * level->getBlockSize()-y_off - (y + offset) ;
                else
                    return level->getBlockSize() - ((y + offset) - j * level->getBlockSize());
            }
            case Direction::Up:
                offset = getCurrentFrame()->getCenterY(d) - getCurrentFrame()->getTopY(d);
                j = (y - offset) / level->getBlockSize();
                return j*level->getBlockSize()- (y - offset);
            case Direction::Right:
                offset = getCurrentFrame()->getBottomX(d) - getCurrentFrame()->getCenterX(d);
                j = (x + offset) / level->getBlockSize();
                dist = j*level->getBlockSize() - (x+offset);
                if (dist<0)
                    dist = 0;
                return dist;
            case Direction::Left:
                offset = getCurrentFrame()->getTopX(d) - getCurrentFrame()->getCenterX(d);
                j = (x + offset) / level->getBlockSize();
                dist = j*level->getBlockSize() - (x+offset);
                if (dist<0)
                    dist = 0;
                return dist;
        }
    } else {
        return 0;
    }
    return 0;
}

void Entity::onFallingUnderMap() {
}

void Entity::onDeath() {
}

void Entity::nextAnimFrame() {
    if (getAction()!=AnimationType::Jumping)
        animationOffset = (animationOffset + 1) % animations->getAnimaionCount(currentAction);
    else {
        if (isJumping && (velocity == 0 || -velocity==max_velocity/2))
            animationOffset+=1;
    }
    lastAnimChange = SDL_GetTicks();

}

void Entity::setAction(AnimationType action) {
    if (currentAction!=action) {
        currentAction = action;
        lastAnimChange = SDL_GetTicks();
        animationOffset = 0;
    }
}

AnimationType Entity::getAction() {
    return currentAction;
}

Texture* Entity::getCurrentFrame() {
    return animations->getAnimation(currentAction, animationOffset);
}

void Entity::setLevel(Level *l) {
    level = l;
}

void Entity::setTickAnimation(bool anim) {
    tickAnimation = anim;
}

void Entity::setDirection(Direction d) {
    direction = d;
}

Direction Entity::getDirection() {
    return direction;
}

int Entity::getTopX(Direction d) {
    if (d==Direction::Current)
        d = direction;
    return x + getCurrentFrame()->getTopX(d) - getCurrentFrame()->getCenterX(d);
}

int Entity::getTopY(Direction d) {
    if (d==Direction::Current)
        d = direction;
    return y + getCurrentFrame()->getTopY(d) - getCurrentFrame()->getCenterY(d);
}

int Entity::getBottomX(Direction d) {
    if (d==Direction::Current)
        d = direction;
    return x + getCurrentFrame()->getBottomX(d) - getCurrentFrame()->getCenterX(d);
}

int Entity::getBottomY(Direction d) {
    if (d==Direction::Current)
        d = direction;
    return y + getCurrentFrame()->getBottomY(d) - getCurrentFrame()->getCenterY(d);
}

int Entity::getStandLeft(Direction d) {
    if (d==Direction::Current)
        d = direction;
    return x + getCurrentFrame()->getStandLeft(direction)-getCurrentFrame()->getCenterX(direction);
}

int Entity::getStandRight(Direction d) {
    return x + getCurrentFrame()->getStandRight(direction)-getCurrentFrame()->getCenterX(direction);
}

bool Entity::isColliding(int x1, int y1, int x2, int y2) {
    return  ((getTopX()<=x1 && x1<=getBottomX()) || (x1<=getTopX() && getTopX()<=x2)) &&
            ((getTopY()<=y1 && y1<=getBottomY()) || (y1<=getTopY() && getTopY()<=y2));
}

bool Entity::isCollinding(Entity *entity) {
    return isColliding(entity->getTopX(), entity->getTopY(), entity->getBottomX(), entity->getBottomY());
}

bool Entity::isCollide(int x, int y, int y_off) {
    if (!level->isBlockSolid(x,y)) return false;
    auto block = level->getBlockTexture(x,y);
    x = x/level->getBlockSize()*level->getBlockSize();
    y = y/level->getBlockSize()*level->getBlockSize();
    return isColliding(x, -block->getTopY(Direction::Current)+y+y_off, block->getBottomX(Direction::Current)+x, block->getBottomY(Direction::Current)+y-block->getTopY(Direction::Current)+y_off);
}

bool Entity::isCollide(Direction d) {
    switch (d) {
        case Direction::Down:
            return isCollide(getStandLeft(), getBottomY()) || isCollide(getStandRight(), getBottomY()) || isCollide(x, getBottomY());
        case Direction::Up:
            return isCollide(getStandLeft(), getTopY()) || isCollide(getStandRight(), getTopY()) || isCollide(x, getTopY());
        case Direction::Left:
            return isCollide(getTopX(), getTopY()) || isCollide(getTopX(), getBottomY()) || isCollide(getTopX(), y);
        case Direction::Right:
            return isCollide(getBottomX(), getTopY()) || isCollide(getBottomX(), getBottomY()) || isCollide(getBottomX(), y);
        case Direction::Current:
            return isCollide(direction);
    }
    return false;
}
