//
// Created by nfs_monstr on 07.08.18.
//

#include <typeinfo>
#include "Chest.h"
#include "Events.h"
#include "Player.h"
#include "Bonus.h"
#include "Level.h"

Chest::Chest() {
    setTickAnimation(false);
}

Chest::Chest(Animations *animation) : Entity(animation) {
    setTickAnimation(false);
}

void Chest::handleEvent(SDL_Event &e) {
    Entity::handleEvent(e);
    switch (e.type) {
        case SDL_USEREVENT:
            switch (e.user.code) {
                case Events::Collide:
                    if (e.user.data1 == nullptr && animationOffset==0 && typeid(*(Entity*)e.user.data2)==typeid(Player) && isCollinding((Entity*)e.user.data2)) {
                        animationOffset++;
                        auto bonus = new Bonus(level->getImageManager()->getAnimation("rd_bonus"));
                        bonus->setPos(x+20, y+10);//todo 
                        level->addEntity(bonus);
                        bonus->jump();
                        e.type = SDL_FIRSTEVENT;
                    }
                    break;
            }
            break;
    }
}