//
// Created by nfs_monstr on 18.08.18.
//

#include "MsgBox.h"

MsgBox::MsgBox(SDL_Renderer *renderer, TextManager *textManager, MsgBoxInfo info, int timeToLive, int offsetTop) : MsgBox(renderer, textManager, info.name, info.text, timeToLive, offsetTop) {
}

MsgBox::MsgBox(SDL_Renderer *renderer, TextManager *textManager, std::string name, std::string text, int timeToLive, int offsetTop) {
    this->name = std::move(name);
    lines = splitToLines(std::move(text), "\n");
    startTime = -1;
    this->renderer = renderer;
    this->textManager = textManager;
    fg_rect.y += offsetTop;
    bg_rect.y += offsetTop;
    if (timeToLive!=-1)
        lifeTime = timeToLive;
    else
        lifeTime = calculateLifeTime();
    startTime = SDL_GetTicks();
    SDL_RenderGetLogicalSize(renderer, &centerX, &centerY);
    centerX/=2;
    centerY/=2;
    bg_rect.x = centerX - bg_rect.w/2 - bg_rect.x;
    fg_rect.x = centerX - fg_rect.w/2 - fg_rect.x;
}

void MsgBox::render() {
    SDL_SetRenderDrawColor(renderer, bg_color.r, bg_color.g, bg_color.b, bg_color.a);
    SDL_RenderFillRect(renderer, &bg_rect);
    SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b, fg_color.a);
    SDL_RenderFillRect(renderer, &fg_rect);
    int y = fg_rect.y + 5;
    int x = fg_rect.w/2+fg_rect.x;
    textManager->getPreparedText(name)->render(fg_rect.x+5, y);
    y+=textManager->getPreparedText(name)->getHeight();
    Texture* text;
    for (const auto& line : lines) {
        y+=5;
        text = textManager->getPreparedText(line.first);
        if (line.second) {
            text->render(x - text->getWidth() / 2, y);
        }
        y+=text->getHeight();
    }
}

void MsgBox::tick() {
}

bool MsgBox::isDone() {
    return SDL_GetTicks()-startTime>lifeTime;
}

int MsgBox::getStartTime() {
    return startTime;
}

std::vector<std::pair<std::string,bool>> MsgBox::splitToLines(std::string text, std::string sep) {
    auto *cstr = const_cast<char *>(text.c_str());
    char *current;
    std::vector<std::pair<std::string,bool>> arr;
    current = strtok(cstr, sep.c_str());
    while (current != nullptr) {
        arr.emplace_back(current,true);//todo add gradually appearing text
        current = strtok(nullptr, sep.c_str());
    }
    return arr;
}

int MsgBox::calculateLifeTime() {
    return 1000 + lines.size()*500;
}