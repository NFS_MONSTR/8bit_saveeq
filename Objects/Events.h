//
// Created by nfs_monstr on 03.08.18.
//

#ifndef INC_8BIT_SAVEEQ_EVENTS_H
#define INC_8BIT_SAVEEQ_EVENTS_H

namespace Events {

    enum EventCode {
        Collide = 0,// это событие не нужно очищать для того, чтобы столкновение происходило со всеми, а не только с первым
        DestroyMe,
        CoinCollected,
        EnemyWasHit,
        EnemyKilled,
        PowerUpPicked,
        Attack,
        RestartLevel,
        CreateMsgBox
    };

    constexpr Entity *to_everyone = nullptr;
}

#endif //INC_8BIT_SAVEEQ_EVENTS_H
