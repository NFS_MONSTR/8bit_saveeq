//
// Created by nfs_monstr on 07.08.18.
//

#ifndef INC_8BIT_SAVEEQ_CHEST_H
#define INC_8BIT_SAVEEQ_CHEST_H


#include "Entity.h"

class Chest : public Entity {
public:
    Chest();
    explicit Chest(Animations *animation);
    void handleEvent(SDL_Event &e) override;
};


#endif //INC_8BIT_SAVEEQ_CHEST_H
