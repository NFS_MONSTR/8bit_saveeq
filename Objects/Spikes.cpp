//
// Created by nfs_monstr on 07.08.18.
//

#include "Spikes.h"
#include "Events.h"
#include "Level.h"

void Spikes::handleEvent(SDL_Event &e) {
    Entity::handleEvent(e);
    switch (e.type) {
        case SDL_USEREVENT:
            switch (e.user.code) {
                case Events::Collide:
                    auto sender = (Entity*)e.user.data2;
                    if (e.user.data1 == nullptr && typeid(*sender)==typeid(Player) && isCollinding(sender)) {
                        level->sendEvent(Events::Attack, sender, this);
                    }
                    break;
            }
            break;
    }
}