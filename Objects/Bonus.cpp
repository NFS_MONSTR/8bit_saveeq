//
// Created by nfs_monstr on 07.08.18.
//

#include "Bonus.h"
#include "Level.h"
#include <cmath>

void Bonus::tick() {

    if (isJumping) {
        velocity+=gravity;
        int offset_x = velocity_x;
        int offset_y = velocity;
        if ((offset_y<0 && isCollide(Direction::Up)) || (offset_y>=0 && isCollide(Direction::Down)))
            offset_y = 0;
        if ((offset_x<0 && isCollide(Direction::Left)) || (offset_x>=0 && isCollide(Direction::Right)))
            offset_x = 0;
        move(offset_x, offset_y);
        if (onGround()) {
            velocity = 0;
            velocity_x = 0;
            isJumping = false;
        }
    } else {
        if (!onGround()) {
            if (!level->isBlockSolid(x, getBottomY() + 10))
                move(0, 10);
            else
                move(0, getDistanceFromNextBlock(Direction::Down));// Чтобы заканчивать падение плавно
        }
    }

    Entity::tick();
}

void Bonus::jump() {
    if (!isJumping && onGround()) {
        isJumping = true;
        velocity_x = (int)std::round(cos(angle)*speed);
        velocity   = -(int)std::round(sin(angle)*speed);
        for (int i = 0; i < 2; ++i)//do two jumps before player collect it
            tick();
    }
}

void Bonus::handleEvent(SDL_Event &e) {
    Entity::handleEvent(e);
    switch (e.type) {
        case SDL_USEREVENT:
            switch (e.user.code) {
                case Events::Collide:
                    if (e.user.data1 == nullptr && typeid(*(Entity*)e.user.data2)==typeid(Player) && isCollinding((Entity*)e.user.data2)) {
                        level->sendEvent(Events::PowerUpPicked, (Entity*)e.user.data2, this);
                        level->sendEvent(Events::DestroyMe, nullptr, this);
                        e.type = SDL_FIRSTEVENT;
                    }
                    break;
            }
            break;
    }
}
