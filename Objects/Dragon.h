//
// Created by nfs_monstr on 14.08.18.
//

#ifndef INC_8BIT_SAVEEQ_DRAGON_H
#define INC_8BIT_SAVEEQ_DRAGON_H


#include "Entity.h"

class Dragon : public Entity {
public:
    using Entity::Entity;

    void handleEvent(SDL_Event &e) override;
    void tick() override;

    void onFallingUnderMap() override;

protected:
    int speed = 5;

};


#endif //INC_8BIT_SAVEEQ_DRAGON_H
