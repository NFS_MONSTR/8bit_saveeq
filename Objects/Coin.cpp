//
// Created by nfs_monstr on 03.08.18.
//

#include "Coin.h"
#include "Events.h"
#include "Level.h"

#include <cmath>
#include <typeinfo>

void Coin::handleEvent(SDL_Event &e) {
    if (e.type == SDL_USEREVENT && e.user.code == Events::Collide && e.user.data1 == nullptr && isCollinding((Entity*)e.user.data2) && typeid(*(Entity*)e.user.data2)==typeid(Player)) {
        level->sendEvent(Events::CoinCollected, (Entity*)e.user.data2, this);
        level->sendEvent(Events::DestroyMe, Events::to_everyone, this);
    }
}
