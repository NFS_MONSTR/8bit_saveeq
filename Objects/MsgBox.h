//
// Created by nfs_monstr on 18.08.18.
//

#ifndef INC_8BIT_SAVEEQ_MSGBOX_H
#define INC_8BIT_SAVEEQ_MSGBOX_H


#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include "../Text/TextManager.h"

struct MsgBoxInfo {
    std::string name;
    std::string text;
    MsgBoxInfo(std::string name, std::string text) {
        this->name = std::move(name);
        this->text = std::move(text);
    }
};

class MsgBox {
public:
    MsgBox(SDL_Renderer *renderer, TextManager *textManager, MsgBoxInfo info, int timeToLive, int offsetTop);
    MsgBox(SDL_Renderer *renderer, TextManager *textManager, std::string name, std::string text, int timeToLive, int offsetTop);
    void render();
    void tick();
    bool isDone();
    int getStartTime();
private:
    std::vector<std::pair<std::string,bool>> splitToLines(std::string text, std::string sep);
    int calculateLifeTime();
    std::string name;
    std::vector<std::pair<std::string,bool>> lines;
    int lifeTime = 0;
    int startTime = 0;
    SDL_Renderer *renderer;
    TextManager *textManager;
    const SDL_Color fg_color = {34, 12, 131, 255};
    const SDL_Color bg_color = {239, 91, 243, 255};
    SDL_Rect fg_rect = {3, 0, 744, 178};
    SDL_Rect bg_rect = {-4, 10, 744, 178};
    int centerX = 0, centerY = 0;
};


#endif //INC_8BIT_SAVEEQ_MSGBOX_H
