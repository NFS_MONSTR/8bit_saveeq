//
// Created by nfs_monstr on 20.08.18.
//

#include <iostream>
#include "Tirek.h"
#include "Events.h"
#include "Level.h"

Tirek::Tirek(Animations *animations) : Entity(animations) {
    speed = 2;
}

void Tirek::tick() {
    if (start_time==0)
        start_time = SDL_GetTicks();
    ai();
    if (attack_cooldown>0)
        attack_cooldown--;
    Entity::tick();
}

void Tirek::go(Direction d) {
    int x_old = x;
    Entity::go(d);
    offset += -(x - x_old);
}

void Tirek::handleEvent(SDL_Event &e) {
    switch (e.type) {
        case SDL_USEREVENT:
            auto sender = (Entity*)e.user.data2;
            switch (e.user.code) {
                case Events::Collide:
                    if (/* todo getAction()==AnimationType::hitted &&*/ typeid(*sender)==typeid(Player) && isCollinding(sender) && attack_cooldown<=0) {
                        level->sendEvent(Events::Attack, sender, this);
                    }
                    break;
                case Events::Attack:
                    if (/* todo getAction()==AnimationType::hitted &&*/ typeid(*sender)==typeid(Player) && isCollinding(sender) && attack_cooldown<=0) {
                        attack_cooldown = attack_cooldown_max;
                        health--;
                        std::cout << health << std::endl;
                        if (health<=0) {
                            level->sendEvent(Events::EnemyKilled, sender, this);
                            //setAction(AnimationType::Jumping); todo set action dying
                            //it's not very good but it works)
                            level->sendEvent(Events::CreateMsgBox, Events::to_everyone, (Entity*)new MsgBoxInfo("tirek","tirek_0\ntirek_1\ntirek_2"));
                            level->sendEvent(Events::DestroyMe, Events::to_everyone, this);//todo temporary
                        } else {
                            level->sendEvent(Events::EnemyWasHit, sender, this);
                            //todo set action hitted
                        }
                    }
                    break;
            }
            break;
    }
}

void Tirek::ai() {
    if (wait_timer<SDL_GetTicks()-start_time) {
        setAction(AnimationType::Walking);
        if (offset >= 186)
            move_dir = Direction::Right;
        if (offset <= -50)
            move_dir = Direction::Left;
        go(move_dir);
    }
}