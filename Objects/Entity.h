//
// Created by nfs_monstr on 27.07.18.
//

#ifndef INC_8BIT_SAVEEQ_ENTITY_H
#define INC_8BIT_SAVEEQ_ENTITY_H


#include "../Textures/Texture.h"
#include "../Textures/Animations.h"

enum class Direction {
    Left,
    Right,
    Up,
    Down,
    Current//Используется только в качестве стандартного параметра, как указание использовать текущий direction
};

class Level;//forward declaration


class Entity {
public:
    Entity() = default;
    explicit Entity(Animations *animation);
    void setTextures(Animations *animation);
    virtual void render();
    virtual void render(int x, int y);
    virtual void renderWithOffset(int offX, int offY);

    virtual void tick();
    virtual void handleEvent(SDL_Event &e);
    virtual void setPos(int x, int y);
    virtual void setSpeed(int speed);
    int getSpeed();
    void move(int x, int y);
    virtual void jump();
    virtual void go(Direction d = Direction::Current);
    virtual void goUpDown(Direction d);
    virtual bool onGround();
    int getDistanceFromNextBlock(Direction d);

    virtual void onFallingUnderMap();
    virtual void onDeath();

    virtual void nextAnimFrame();
    void setAction(AnimationType action);
    AnimationType getAction();
    Texture* getCurrentFrame();
    void setLevel(Level *l);
    void setTickAnimation(bool anim);
    void setDirection(Direction d);
    Direction getDirection();
    int getX() {return x;};
    int getY() {return y;};
    int getTopX(Direction d = Direction::Current);
    int getTopY(Direction d = Direction::Current);
    int getBottomX(Direction d = Direction::Current);
    int getBottomY(Direction d = Direction::Current);
    int getStandLeft(Direction d = Direction::Current);
    int getStandRight(Direction d = Direction::Current);
    bool isColliding(int x1, int y1, int x2, int y2);
    bool isCollinding(Entity *entity);
    bool isCollide(int x, int y, int y_off = 0);
    bool isCollide(Direction d = Direction::Current);


protected:
    int x = 0, y = 0;


    AnimationType currentAction = AnimationType::Standing;
    int animationOffset = 0;
    int lastAnimChange = 0;
    int gravity = 1;
    int velocity = 0;
    int max_velocity = -22;
    int speed = 10;

    bool isJumping = false;
    Animations *animations = nullptr;
    bool tickAnimation = true;
    Direction direction = Direction::Right;
    Level *level = nullptr;
    int health = 100;
};


#endif //INC_8BIT_SAVEEQ_ENTITY_H
