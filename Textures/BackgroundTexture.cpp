//
// Created by nfs_monstr on 18.07.18.
//

#include <SDL_image.h>
#include "BackgroundTexture.h"

bool BackgroundTexture::loadImage(std::string path, int centerX, int centerY) {
    freeImage();
    SDL_Texture * newTexture = nullptr;
    SDL_Surface * lSurf = IMG_Load(path.c_str());
    if (lSurf == nullptr) {
        std::printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
    } else {

        SDL_Texture * tex = SDL_CreateTextureFromSurface(mRenderer, lSurf);
        newTexture = SDL_CreateTexture(mRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, lSurf->w, lSurf->h);
        SDL_SetRenderTarget(mRenderer, newTexture);
        SDL_RenderCopy(mRenderer, tex, nullptr, nullptr);
        SDL_SetRenderTarget(mRenderer, nullptr);
        SDL_DestroyTexture(tex);
        mWidth = lSurf->w; mHeight = lSurf->h;
        SDL_FreeSurface(lSurf);

    }
    mTexture = newTexture;
    center.x = centerX;
    center.y = centerY;
    return mTexture != nullptr;
}

void BackgroundTexture::renderToTexture(Texture *what, int x, int y, SDL_Rect *clip, double angle) {
    if (mTexture != nullptr && what != nullptr) {
        if (SDL_SetRenderTarget(mRenderer, mTexture) == 0) {
            what->render(x, y, clip, angle);
            SDL_SetRenderTarget(mRenderer, nullptr);
        } else
            std::printf(SDL_GetError());
    }
}
