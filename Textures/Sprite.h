//
// Created by nfs_monstr on 27.07.18.
//

#ifndef INC_8BIT_SAVEEQ_SPRITE_H
#define INC_8BIT_SAVEEQ_SPRITE_H


#include "Texture.h"

class Sprite : public Texture {
public:
    Sprite(SDL_Renderer *renderer, std::string path = "", std::string configPath = "");
    virtual bool loadSprite(std::string path, std::string configPath);

protected:
    void loadConfig(std::string configPath);
};


#endif //INC_8BIT_SAVEEQ_SPRITE_H
