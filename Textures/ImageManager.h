//
// Created by nfs_monstr on 06.08.18.
//

#ifndef INC_8BIT_SAVEEQ_IMAGEMANAGER_H
#define INC_8BIT_SAVEEQ_IMAGEMANAGER_H


#include "Texture.h"
#include "Animations.h"

class ImageManager {
public:
    explicit ImageManager(SDL_Renderer *renderer);
    ~ImageManager();
    void addImage(Texture *img, std::string name);
    void addImage(std::string path, std::string name);
    Texture* getImage(std::string name);
    void destroyImage(std::string name);

    void addAnimation(Animations *animation, std::string name);
    Animations* getAnimation(std::string name);
    void destroyAnimation(std::string name);

protected:
    std::map<std::string,Texture*> images;
    std::map<std::string,Animations*> animations;
    SDL_Renderer *renderer = nullptr;
};


#endif //INC_8BIT_SAVEEQ_IMAGEMANAGER_H
