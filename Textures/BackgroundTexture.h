//
// Created by nfs_monstr on 18.07.18.
//

#ifndef EightBit_SaveEq_BACKGROUNDTEXTURE_H
#define EightBit_SaveEq_BACKGROUNDTEXTURE_H


#include "Texture.h"

class BackgroundTexture : public Texture {
    using Texture::Texture;
public:
    bool loadImage(std::string path, int centerX = 0, int centerY = 0);
    void renderToTexture(Texture* what, int x, int y, SDL_Rect *clip = nullptr, double angle = 0.0);

};


#endif
