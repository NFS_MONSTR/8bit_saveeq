//
// Created by nfs_monstr on 17.07.18.
//

#include <SDL_image.h>
#include "Texture.h"

#include "../Objects/Entity.h"//todo move Direction from Entity.h in separate file

Texture::Texture(SDL_Renderer *renderer, std::string path) {
    mTexture = nullptr;
    mWidth = 0;
    mHeight = 0;
    mRenderer = renderer;
    hitbox = {0, 0, 0, 0};
    mStandLeft = 0;
    mStandRight = 0;
    mCycleWidth = 764;

    if (!path.empty())
        loadImage(path);
}

Texture::Texture(SDL_Renderer *renderer, SDL_Texture *texture, int width, int height, int cycleWidth) {
    mTexture = texture;
    mWidth = width;
    mHeight = height;
    mRenderer = renderer;
    hitbox = {0, 0, 0, 0};
    mStandLeft = 0;
    mStandRight = 0;
    center.x = width/2;
    center.y = height/2;
    mCycleWidth = cycleWidth;
}

Texture::~Texture() {
    freeImage();
}

bool Texture::loadImage(std::string path, int centerX, int centerY) {
    freeImage();
    if (path!="none") {
        SDL_Texture *newTexture = nullptr;
        SDL_Surface *lSurf = IMG_Load(path.c_str());
        if (lSurf == nullptr) {
            std::printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
        } else {
            newTexture = SDL_CreateTextureFromSurface(mRenderer, lSurf);
            if (newTexture == nullptr) {
                std::printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
            } else {
                //Get image dimensions
                mWidth = lSurf->w;
                mHeight = lSurf->h;
            }
            SDL_FreeSurface(lSurf);
        }
        mTexture = newTexture;
        center.x = centerX;
        center.y = centerY;
        hitbox = {0, 0, mWidth, mHeight};
        return mTexture != nullptr;
    } else {
        mTexture = nullptr;
        return true;
    }
}


void Texture::setColorMod( Uint8 red, Uint8 green, Uint8 blue ) {
    if (mTexture != nullptr)
        SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void Texture::setBlendMode( SDL_BlendMode blending ) {
    if (mTexture != nullptr)
        SDL_SetTextureBlendMode(mTexture, blending);
}

void Texture::setAlphaMod( Uint8 alpha ) {
    if (mTexture != nullptr)
        SDL_SetTextureAlphaMod(mTexture, alpha);
}

void Texture::render(int x, int y, SDL_Rect *clip, double angle, SDL_RendererFlip flip) {
    if (mTexture != nullptr) {
        //Set rendering space and render to screen
        SDL_Rect renderQuad = {x, y, mWidth, mHeight};
        //Set clip rendering dimensions
        if (clip != nullptr)
            renderQuad = *clip;

        //Render to screen
        SDL_RenderCopyEx(mRenderer, mTexture, clip, &renderQuad, angle, &center, flip);
    }
}

void Texture::renderCentered(int x, int y, SDL_Rect *clip, double angle, SDL_RendererFlip flip) {
    int xC;
    if (flip & SDL_FLIP_HORIZONTAL!=0) {
        xC = x - getCenterX(Direction::Left);
    } else {
        xC = x - getCenterX(Direction::Right);
    }
    render(xC, y-center.y, clip, angle, flip);
}

int Texture::getWidth() {
    return mWidth;
}

int Texture::getHeight() {
    return mHeight;
}

int Texture::getTopX(Direction d) {
    return d==Direction::Left ? mWidth-hitbox.w : hitbox.x;
}

int Texture::getTopY(Direction d) {
    return hitbox.y;
}

int Texture::getBottomX(Direction d) {
    return d==Direction::Left ? mWidth-hitbox.x : hitbox.w;
}

int Texture::getBottomY(Direction d) {
    return hitbox.h;
}

SDL_Rect Texture::getHitbox() {
    return hitbox;
}

int Texture::getCenterX(Direction d) {
    return d==Direction::Left ? mWidth-1-center.x : center.x;
}

int Texture::getCenterY(Direction d) {
    return center.y;
}

int Texture::getStandLeft(Direction d) {//todo move stand to entity
    return d==Direction::Left ? mWidth-mStandRight-1 : mStandLeft;
}

int Texture::getStandRight(Direction d) {
    return d==Direction::Left ? mWidth-mStandLeft-1 : mStandRight;
}

int Texture::getCycleWidth() {
    return mCycleWidth;
}

void Texture::setSolid(bool s) {
    mSolid = s;
}

bool Texture::isSolid() {
    return mSolid;
}

void Texture::freeImage() {
    if (mTexture != nullptr) {
        SDL_DestroyTexture(mTexture);
        mTexture = nullptr;
        mWidth = 0;
        mHeight = 0;
    }
}
