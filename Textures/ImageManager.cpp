//
// Created by nfs_monstr on 06.08.18.
//

#include "ImageManager.h"

ImageManager::ImageManager(SDL_Renderer *renderer) {
    this->renderer = renderer;
}

ImageManager::~ImageManager() {
    for (auto item : images)
        delete item.second;
    for (auto item : animations)
        delete item.second;
}

void ImageManager::addImage(Texture *img, std::string name) {
    images.insert(std::pair<std::string,Texture*>(name, img));
}

void ImageManager::addImage(std::string path, std::string name) {
    Texture* texture = new Texture(renderer, std::move(path));
    addImage(texture, std::move(name));
}

Texture* ImageManager::getImage(std::string name) {
    return images[name];
}

void ImageManager::destroyImage(std::string name) {
    delete images[name];
    images.erase(name);
}

void ImageManager::addAnimation(Animations *animation, std::string name) {
    animations.insert(std::pair<std::string,Animations*>(name,animation));
}

Animations* ImageManager::getAnimation(std::string name) {
    return animations[name];
}

void ImageManager::destroyAnimation(std::string name) {
    delete animations[name];
    animations.erase(name);
}