//
// Created by nfs_monstr on 27.07.18.
//

#ifndef INC_8BIT_SAVEEQ_ANIMATIONS_H
#define INC_8BIT_SAVEEQ_ANIMATIONS_H

#include <map>
#include <vector>
#include "Texture.h"

enum class AnimationType {
    Standing,
    Walking,
    Jumping,
    Flying,
    Prepare_PowerUp,
    PowerUp,
    Attacking
};


class Animations {
public:
    Animations() = default;
    ~Animations();
    void addAnimation(AnimationType animationType, Texture *animation);
    void addAnimation(AnimationType animationType, SDL_Renderer* renderer, std::string file);
    void addAnimation(AnimationType animationType, SDL_Renderer* renderer, std::string file, std::string config);
    void setDelay(AnimationType animationType, int delay);
    int  getDelay(AnimationType animationType = AnimationType::Standing);
    unsigned long getAnimaionCount(AnimationType animationType = AnimationType::Standing);
    Texture* getAnimation(AnimationType animationType, unsigned long animationNumber);

private:
    std::map<AnimationType, std::pair<int,std::vector<Texture*>>> animations;
};


#endif //INC_8BIT_SAVEEQ_ANIMATIONS_H
