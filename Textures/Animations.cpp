//
// Created by nfs_monstr on 27.07.18.
//

#include "Animations.h"
#include "Sprite.h"


Animations::~Animations() {
    for ( auto const& anim : animations ) {
        for (auto const& img : anim.second.second) {
            delete img;
        }
    }


}

void Animations::addAnimation(AnimationType animationType, Texture *animation) {
    animations[animationType].second.push_back(animation);
    animations[animationType].first = 0;
}

void Animations::addAnimation(AnimationType animationType, SDL_Renderer* renderer, std::string file) {
    auto *img = new Texture(renderer,std::move(file));
    animations[animationType].second.push_back(img);
    animations[animationType].first = 0;
}

void Animations::addAnimation(AnimationType animationType, SDL_Renderer* renderer, std::string file, std::string config) {
    auto *img = new Sprite(renderer,std::move(file),std::move(config));
    animations[animationType].second.push_back(img);
    animations[animationType].first = 0;
}

void Animations::setDelay(AnimationType animationType, int delay) {
    animations[animationType].first = delay;
}

int Animations::getDelay(AnimationType animationType) {
    return animations[animationType].first;
}

unsigned long Animations::getAnimaionCount(AnimationType animationType) {
    return animations[animationType].second.size();
}

Texture* Animations::getAnimation(AnimationType animationType, unsigned long animationNumber) {
    if (animationNumber<animations[animationType].second.size())
        return animations[animationType].second.at(animationNumber);
    else
        return nullptr;
}