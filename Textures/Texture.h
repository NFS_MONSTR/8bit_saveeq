//
// Created by nfs_monstr on 17.07.18.
//

#ifndef EightBit_SaveEq_TEXTURE_H
#define EightBit_SaveEq_TEXTURE_H


#include <string>
#include <SDL2/SDL.h>

enum class Direction;//forward

class Texture {
public:
    //Initializes variables
    explicit Texture(SDL_Renderer *renderer, std::string path = "");
    Texture(SDL_Renderer *renderer, SDL_Texture *texture, int width = 0, int height = 0, int cycleWidth = 0);

    //Deallocates memory
    virtual ~Texture();

    virtual bool loadImage(std::string path, int centerX = 0, int centerY = 0);

    //Set color modulation
    void setColorMod(Uint8 red, Uint8 green, Uint8 blue);

    //Set blending
    void setBlendMode(SDL_BlendMode blending);

    //Set alpha modulation
    void setAlphaMod(Uint8 alpha);

    //Renders texture at given point
    void render(int x, int y, SDL_Rect *clip = nullptr, double angle = 0.0, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void renderCentered(int x, int y, SDL_Rect *clip = nullptr, double angle = 0.0, SDL_RendererFlip flip = SDL_FLIP_NONE);

    //Gets image dimensions
    int getWidth();
    int getHeight();
    int getTopX(Direction d);
    int getTopY(Direction d);
    int getBottomX(Direction d);
    int getBottomY(Direction d);
    SDL_Rect getHitbox();
    int getCenterX(Direction d);
    int getCenterY(Direction d);
    int getStandLeft(Direction d);
    int getStandRight(Direction d);
    int getCycleWidth();

    void setSolid(bool s);
    bool isSolid();


protected:

    void freeImage();

    //The actual hardware texture
    SDL_Texture *mTexture;

    //Image dimensions
    int mWidth;
    int mHeight;
    SDL_Point center;

    SDL_Rect hitbox;
    int mStandLeft;
    int mStandRight;
    int mCycleWidth;
    bool mSolid = true;


    SDL_Renderer *mRenderer;
};


#endif
