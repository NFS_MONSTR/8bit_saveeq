//
// Created by nfs_monstr on 27.07.18.
//

#include "Sprite.h"
#include "../Config/Config.h"

Sprite::Sprite(SDL_Renderer *renderer, std::string path, std::string configPath) : Texture(renderer, path) {
    if (!configPath.empty()) {
        loadConfig(std::move(configPath));
    } else {
        if (path!="none" && !path.empty()) {
            path.replace(path.length() - 3, 3, "cfg");
            loadConfig(path);
        }
    }
}

bool Sprite::loadSprite(std::string path, std::string configPath) {
    bool loadRes = loadImage(std::move(path));
    loadConfig(std::move(configPath));
    return loadRes;
}

void Sprite::loadConfig(std::string configPath) {
    Config config;
    config.load(std::move(configPath));

    config.setIntIfExists("center_x", center.x);
    config.setIntIfExists("center_y", center.y);
    config.setIntIfExists("width", mWidth);
    config.setIntIfExists("height", mHeight);
    config.setIntIfExists("top_x", hitbox.x);
    config.setIntIfExists("top_y", hitbox.y);
    config.setIntIfExists("bottom_x", hitbox.w);
    config.setIntIfExists("bottom_y", hitbox.h);
    config.setIntIfExists("stand_left", mStandLeft);
    config.setIntIfExists("stand_right", mStandRight);
    config.setBoolIntIfExists("solid", mSolid);
    config.setIntIfExists("cycle_width", mCycleWidth);
}
